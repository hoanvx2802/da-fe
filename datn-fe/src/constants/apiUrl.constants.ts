export const apiUrls = {
	login: "/login",
	refreshToken: "/refreshToken",
	changePassword: "/change-password",
	forgotPassword: "/forgot-password",
	active: "/active",
	register: "/register",
	user: "/user",

	admin: "/admin",

	contract: "/contract",
	contractYc: "/contract/yc",
	acceptContract: "/contract/accept-or-reject",
	extendContract: "/contract/extend",

	employee: "/employee",
	employeeAccept: "accept-by",

	request: "/request",
	requestYc: "/request/yc",
	requestAction: "/request/accept-or-reject",

	room: "/room",

	statitics: "/statitics",
	statiticRoom: "/statitics/room",

	student: "/student",
	studentInRoom: "student-in-room",

	system: "/system",

	waterAndElectric: "/utilities",
	fetchWAE: "fetch-wae",
	uploadImg: "/public/image/avt",

	cicout: "/check",
};
