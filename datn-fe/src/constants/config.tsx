import {
	CheckOutlined,
	CloseOutlined,
	DeleteOutlined,
	EditOutlined,
	InfoCircleOutlined,
} from "@ant-design/icons";

export const ROLE = {
	ADMIN: "ADMIN",
	MANAGEMENT_FINANCIAL: "MANAGEMENT_FINANCIAL", // NV tài chính
	ADMINISTRATIVE: "ADMINISTRATIVE", // NV hành chính
	STUDENT: "STUDENT",
} as const;

export const DEFAULT_QUERY = {
	page: 1,
	limit: 10,
} as const;

export const MODAL_TYPE = {
	ADD: undefined,
	EDIT: "EDIT",
	DETAIL: "DETAIL",
} as const;

export const ROOM_STATUS = {
	EMPTY: "CHUA_CO_NGUOI",
	FULL: "DU_NGUOI",
	NOT_FULL: "THIEU_NGUOI",
} as const;

export const ROOM_STATUS_OPTIONS = [
	{ label: "Chưa có người", value: ROOM_STATUS.EMPTY },
	{ label: "Đủ người", value: ROOM_STATUS.FULL },
	{ label: "Thiếu người", value: ROOM_STATUS.NOT_FULL },
] as const;

export const EMPLOYEE_POSITION_OPTIONS = [
	{ label: "Admin", value: ROLE.ADMIN },
	{ label: "Quản lý hành chính", value: ROLE.ADMINISTRATIVE },
	{ label: "Quản lý tài chính", value: ROLE.MANAGEMENT_FINANCIAL },
] as const;

export const CONTRACT_STATUS_OPTIONS = [
	{ label: "Đã được duyệt", value: "APPROVED" },
	{ label: "Từ chối", value: "REJECTED" },
	{ label: "Đã hủy", value: "CANCELED" },
	// { label: "Chờ duyệt", value: "PENDING" },
	{ label: "Hết hạn", value: "TIME_OUT" },
] as const;

export const STATUS_OPTIONS = [
	{ label: "Đã được duyệt", value: "APPROVED" },
	{ label: "Từ chối", value: "REJECTED" },
	{ label: "Đã hủy", value: "CANCELED" },
	{ label: "Chờ duyệt", value: "PENDING" },
	{ label: "Hết hạn", value: "TIME_OUT" },
] as const;


export const FORMAT_DATE = "YYYY-MM-DD";
export const FORMAT_DATE_TIME = "HH:mm DD/MM/YYYY";
export const DISPLAY_DATE_FORMAT = "DD/MM/YYYY";

// form
export const RULES = {
	REQUIRED: {
		required: true,
	},
	EMAIL: {
		required: true,
		type: "email",
	},
	PHONE_NUMBER: {
		pattern: new RegExp(/(84|0[3|5|7|8|9])+([0-9]{8})\b/),
		message: "Số điện thoại không hợp lệ!",
	},
	NUMBER: {
		type: "number",
	},
} as const;

// table
export const TABLE_ACTIONS = {
	EDIT: MODAL_TYPE.EDIT,
	DETAIL: MODAL_TYPE.DETAIL,
	DELETE: "DELETE",
	APPROVED: "APPROVED",
	REJECTED: "REJECTED",
	CANCELED: "CANCELED",
} as const;

export const TABLE_ACTION_APPROVE_LABELS = {
	[TABLE_ACTIONS.APPROVED]: "Duyệt",
	[TABLE_ACTIONS.REJECTED]: "Từ chối",
	[TABLE_ACTIONS.CANCELED]: "Hủy",
} as const;

export const TABLE_ACTION_ITEMS = {
	DETAIL: {
		key: TABLE_ACTIONS.DETAIL,
		className: "w-[180px]",
		label: "Chi tiết",
		icon: <InfoCircleOutlined />,
	},
	EDIT: {
		key: TABLE_ACTIONS.EDIT,
		label: "Chỉnh sửa",
		icon: <EditOutlined />,
	},
	DELETE: {
		key: TABLE_ACTIONS.DELETE,
		label: "Xóa",
		danger: true,
		icon: <DeleteOutlined />,
	},
	APPROVED: {
		key: TABLE_ACTIONS.APPROVED,
		label: "Duyệt",
		icon: <CheckOutlined />,
		className: "!text-green-500",
	},
	REJECTED: {
		key: TABLE_ACTIONS.REJECTED,
		label: "Từ chối",
		icon: <CloseOutlined />,
		className: "!text-red-500",
	},
	CANCELED: {
		key: TABLE_ACTIONS.CANCELED,
		label: "Hủy",
		icon: <CloseOutlined />,
		danger: true,
	},
} as const;

export const DEFAULT_TABLE_ACTIONS = [
	TABLE_ACTION_ITEMS.DETAIL,
	TABLE_ACTION_ITEMS.EDIT,
	TABLE_ACTION_ITEMS.DELETE,
] as const;
