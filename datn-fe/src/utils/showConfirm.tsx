import { ExclamationCircleFilled } from '@ant-design/icons';
import { Modal } from 'antd';

const { confirm } = Modal;

const showConfirm: typeof confirm = ({onOk, ...props}) => {
  return confirm({
    centered: true,
      icon: <ExclamationCircleFilled />,
      title: 'Bạn có chắc muốn xóa sinh viên này không?',
    content: 'Hành động này không thể khôi phục',
    onOk,
    ...props,
  });
};

export default showConfirm;