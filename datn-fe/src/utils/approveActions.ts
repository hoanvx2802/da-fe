import { ItemType } from "antd/es/menu/hooks/useItems";
import {
	DEFAULT_TABLE_ACTIONS,
	TABLE_ACTION_ITEMS,
	TABLE_ACTIONS,
} from "~/constants/config";

export const getApproveActions = (record: any) => {
	console.log(JSON.stringify(record))
	const items: ItemType[] = [TABLE_ACTION_ITEMS.DETAIL];
	if (record.status === TABLE_ACTIONS.APPROVED) {
		items.push(TABLE_ACTION_ITEMS.CANCELED);
	} else if (record.status !== TABLE_ACTIONS.REJECTED && record.status !== TABLE_ACTIONS.CANCELED) {
		items.push(TABLE_ACTION_ITEMS.APPROVED, TABLE_ACTION_ITEMS.REJECTED);
	}
	return items;
};

export const getActionsDependStatus = (record: any): ItemType[] => {
	if (record.status === "PENDING") {
		return [...DEFAULT_TABLE_ACTIONS];
	}
	return [TABLE_ACTION_ITEMS.DETAIL];
};
