import { authKeys } from "../global/keys/authKeys";

export const gotoLogin = () => {
	const url = new URL(window.location.href);
	const rawPath = url.origin + url.pathname;
	localStorage.clear();
	window.location.href = `${
		import.meta.env.VITE_APP_LOGIN_PORTAL_URL
	}?redirect=${encodeURIComponent(rawPath)}`;
};

export const removeTokenOnUrl = () => {
	const url = new URL(window.location.href);
	const token = url.searchParams.get(authKeys.token);
	if (token) {
		url.searchParams.delete(authKeys.token);
		window.history.replaceState({}, document.title, url.href);
	}
};
