import { authKeys } from "../global/keys/authKeys";

export const authenUtils = {
	getToken() {
		return localStorage.getItem(authKeys.accessToken) ?? undefined;
	},
	setToken(token: string) {
		localStorage.setItem(authKeys.token, token);
	},
	removeToken() {
		localStorage.removeItem(authKeys.token);
	}
};
