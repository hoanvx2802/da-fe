import dayjs, { Dayjs } from "dayjs";
import queryString from "query-string";
import {
	DISPLAY_DATE_FORMAT,
	FORMAT_DATE,
	FORMAT_DATE_TIME,
} from "~/constants/config";

export const paramsSerializer = (params: Record<string, any>) =>
	queryString.stringify(params, {
		skipNull: true,
		skipEmptyString: true,
	});

export const parseQuery = (query: string) =>
	queryString.parse(query, {
		parseNumbers: true,
		parseBooleans: true,
	});

export const convertQueryWithTimeRange = ({
	range,
	...params
}: Record<string, any>) => {
	if (!range) {
		return params;
	}
	return {
		...params,
		from: range[0],
		to: range[1],
	};
};

export const stringToDate = (date?: Dayjs | string | null) => {
	return date ? dayjs(date, FORMAT_DATE) : undefined;
};

export const formatDate = (date?: Dayjs | null) => {
	return date ? dayjs(date).format(FORMAT_DATE) : undefined;
};

export const formatDateTime = (date?: Dayjs | null) => {
	return date ? dayjs(date).format(FORMAT_DATE_TIME) : undefined;
};

export const formatDisplayDate = (date?: Dayjs | null) => {
	return date ? dayjs(date).format(DISPLAY_DATE_FORMAT) : undefined;
};
