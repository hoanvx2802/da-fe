import { apiUrls } from "~/constants/apiUrl.constants";
import { axiosClient } from "./axiosConfig/axiosClient";

export const uploadImage = async (file: File) => {
	const formData = new FormData();
	formData.append("file", file);

	return axiosClient
		.post(apiUrls.uploadImg, formData, {
			headers: {
				"Content-Type": "multipart/form-data",
			},
		})
		.then((res) => res.data);
};

export const getImage = async (filename: string, key: string = "avt") => {
	return axiosClient
		.get(apiUrls.uploadImg, {
			params: {
				filename,
				key,
			},
		})
		.then((res) => res.data);
};

export const convertGetData = async (
	data: any,
	imageUrlKey: string,
	imageFieldKey: string,
	key?: string
) => {
	let imageUrl = data[imageUrlKey];
	if (imageUrl) {
		imageUrl = await getImage(imageUrl, key);
	}
	return {
		...data,
		[imageFieldKey]: {
			url: imageUrl,
		},
	};
};

export const revertData = async (
	data: any,
	imageUrlKey: string,
	imageFieldKey: string
) => {
	let imageUrl = data[imageUrlKey];
	if (data[imageFieldKey]?.file) {
		imageUrl = await uploadImage(data[imageFieldKey].file);
	}
	const result = {
		...data,
		[imageUrlKey]: imageUrl,
	};
	delete result[imageFieldKey];
	return result;
};
