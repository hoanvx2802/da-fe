import { apiUrls } from "../constants/apiUrl.constants";
import { axiosClient } from "./axiosConfig/axiosClient";

export type LoginValues = {
	username: string;
	password: string;
};

export const loginApis = {
	async login(values: LoginValues) {
		const result: any = await axiosClient.post(apiUrls.login, values, {
			_auth: false,
		});
		return result.data;
	},
};
