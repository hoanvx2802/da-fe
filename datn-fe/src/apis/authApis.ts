import { apiUrls } from "../constants/apiUrl.constants";
import { axiosClient } from "./axiosConfig/axiosClient";

export const authApis = {
	async systemConfig(values: any) {
		return axiosClient.post(apiUrls.system, {
			values,
		});
	},

	async changePassword(values: any) {
		return axiosClient.post(apiUrls.changePassword, {
			values,
		});
	},

	async active(values: any) {
		return axiosClient.post(apiUrls.active, {
			values,
		});
	},

	async register(values: any) {
		const result: any = await axiosClient.post(apiUrls.register, values, {
			_auth: false,
		});
		return result.data;
	},

	async forgotPassword(values: any) {
		const result: any = await axiosClient.post(apiUrls.forgotPassword, values, {
			_auth: false,
		});
		return result.data;
	},
};
