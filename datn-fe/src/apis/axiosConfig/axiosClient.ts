import axios, { AxiosError, AxiosInstance, AxiosRequestConfig } from "axios";
import { paramsSerializer } from "~/utils/convert";
import { authenUtils } from "../../utils/localStorage.utils";
import { headerAxios } from "./axiosConfig";
import { message } from "antd";

declare module "axios" {
	export interface AxiosRequestConfig {
		_auth?: boolean;
	}
}

// Create a custom Axios client
export const axiosClient: AxiosInstance = axios.create({
	baseURL: import.meta.env.VITE_APP_BASE_URL,
	headers: headerAxios,
	paramsSerializer,
});

axiosClient.interceptors.request.use(function (
	config: AxiosRequestConfig<any>
) {
	const token = authenUtils.getToken();
	if (token && config._auth !== false) {
		config.headers = {
			...config?.headers,
			Authorization: "Bearer " + token,
		};
	}
	return config;
} as any);

axiosClient.interceptors.response.use(
	(response) => {
		// Handle successful responses
		return response;
	},
	(error: AxiosError) => {
		if (error.response?.status === 401) {
			message.error("Bạn không có quyền cho chức năng này");
		}

		return Promise.reject(error.response?.data);
	}
);
