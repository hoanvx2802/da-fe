import { Avatar, Col, Form, Input, Row } from "antd";

import useTransformImage from "~/hooks/useTransformImage";
import ModalForm from "./form/ModalForm";
import { formatDateTime } from "~/utils/convert";

const ModalCiCout = () => {
	const convertData = useTransformImage({
		imageUrlKey: "image",
		convert: (data) => ({
			...data,
			timeCheck: formatDateTime(data.timeCheck),
		}),
		key: "checkinout",
	});
	return (
		<>
			<ModalForm
				title="vào ra"
				modalProps={{
					width: 800,
				}}
				mutationFn={(values: any) => {
					return values;
				}}
				convertData={convertData}
				layout="vertical"
				labelCol={{
					span: 24,
				}}>
				<Row gutter={16}>
					<Col span={16}>
						<Form.Item noStyle name="image" valuePropName="src">
							<img className="w-full h-full" />
						</Form.Item>
					</Col>
					<Col span={8}>
						<Form.Item label="Tên sinh viên" name="studentFullName">
							<Input />
						</Form.Item>
						<Form.Item label="Thời gian" name="timeCheck">
							<Input />
						</Form.Item>
						<Form.Item label="Trạng thái đi vào" name="status">
							<Input />
						</Form.Item>
					</Col>
				</Row>
			</ModalForm>
		</>
	);
};

export default ModalCiCout;
