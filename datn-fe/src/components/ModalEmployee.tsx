import { Form, Input, Radio, Select } from "antd";
import { revertData } from "~/apis/upload";
import { apiUrls } from "~/constants/apiUrl.constants";
import { MODAL_TYPE, RULES } from "~/constants/config";
import { useTransformImageDefault } from "~/hooks/useTransformImage";
import { useGetModalType } from "~/stores/modalStore";
import { axiosClient } from "../apis/axiosConfig/axiosClient";
import AppDatePicker from "./fields/AppDatePicker";
import PasswordField from "./fields/PasswordField";
import UploadAvatar from "./fields/UploadAvatar";
import ModalForm from "./form/ModalForm";

const ModalEmployee = () => {
  const modalType = useGetModalType();
  const convertGetData = useTransformImageDefault();
  return (
    <ModalForm
      title="nhân viên"
      convertData={convertGetData}
      mutationFn={async (values: any) => {
        const employee = await revertData(values, "imageUrl", "avatar");
        if (employee.id) {
          return axiosClient.put(apiUrls.employee, employee);
        } else {
          return axiosClient.post(apiUrls.employee, employee);
        }
      }}
      queryKey="UseEmployee"
    >
      <Form.Item name="avatar">
        <UploadAvatar />
      </Form.Item>
      {(modalType === MODAL_TYPE.DETAIL || modalType === MODAL_TYPE.EDIT) && (
        <>
          <Form.Item
            label="Tên đăng nhập"
            rules={[RULES.REQUIRED]}
            name={"username"}
          >
            <Input disabled={true} />
          </Form.Item>
        </>
      )}
      {modalType === MODAL_TYPE.ADD && <PasswordField />}
      <Form.Item label="Họ và tên" rules={[RULES.REQUIRED]} name={"fullName"}>
        <Input placeholder="Nhập họ và tên" />
      </Form.Item>
      <Form.Item label="Email" rules={[RULES.EMAIL]} name={"email"}>
        <Input placeholder="Nhập email" />
      </Form.Item>
      <Form.Item label="Chức vụ" rules={[RULES.REQUIRED]} name={"role"}>
        <Select placeholder="Chọn chức vụ">
          <Select.Option value="MANAGEMENT_FINANCIAL">
            Quản lý tài chính
          </Select.Option>
          <Select.Option value="ADMINISTRATIVE">
            Quản lý hành chính
          </Select.Option>
        </Select>
      </Form.Item>
      <Form.Item label="Ngày bắt đầu làm" name={"onBoard"}>
        <AppDatePicker />
      </Form.Item>
      <Form.Item label="Ngày kết thúc làm" name={"offBoard"}>
        <AppDatePicker />
      </Form.Item>
      <Form.Item label="Giới tính" name={"sex"}>
        <Radio.Group>
          <Radio value={true}>Nam</Radio>
          <Radio value={false}>Nữ</Radio>
        </Radio.Group>
      </Form.Item>
      <Form.Item label="Số điện thoại" name={"phoneNumber"}>
        <Input placeholder="Nhập số điện thoại" />
      </Form.Item>
	  <Form.Item
        labelCol={{
          span: 24,
        }}
        wrapperCol={{
          span: 24,
        }}
        label="Địa chỉ"
        name="address"
      >
        <Input.TextArea rows={4} />
      </Form.Item>
	  <Form.Item label="Số căn cước" name={"citizenIdentificationCard"}>
        <Input placeholder="Nhập số căn cước" />
      </Form.Item>
    </ModalForm>
  );
};

export default ModalEmployee;
