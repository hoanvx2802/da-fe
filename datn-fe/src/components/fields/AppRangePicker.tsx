import { DatePicker, DatePickerProps } from "antd";
import { RangePickerProps } from "antd/es/date-picker";
import { DISPLAY_DATE_FORMAT } from "~/constants/config";
import { formatDate, stringToDate } from "~/utils/convert";

type RangePickerValue = [string, string];
export interface AppDatePickerProps
	extends Omit<RangePickerProps, "onChange" | "value"> {
	onChange?: (value?: RangePickerValue) => void;
	value?: RangePickerValue;
}

const AppRangePicker = ({
	className = "",
	onChange,
	value,
	...props
}: AppDatePickerProps) => {
	return (
		<DatePicker.RangePicker
			value={
				value ? [stringToDate(value[0]), stringToDate(value[1])] : undefined
			}
			onChange={(date) => {
				onChange?.(
					date ? [formatDate(date[0])!, formatDate(date[1])!] : undefined
				);
			}}
			className={`w-full ${className}`}
			placeholder={["Từ ngày", "Đến ngày"]}
			format={DISPLAY_DATE_FORMAT}
			{...props}
		/>
	);
};

export default AppRangePicker;
