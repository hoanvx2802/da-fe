import { DatePicker, DatePickerProps } from "antd";
import { DISPLAY_DATE_FORMAT } from "~/constants/config";
import { formatDate, stringToDate } from "~/utils/convert";

export interface AppDatePickerProps
	extends Omit<DatePickerProps, "onChange" | "value"> {
	onChange?: (value?: string) => void;
	value?: string;
}

const AppDatePicker = ({
	className = "",
	onChange,
	value,
	...props
}: AppDatePickerProps) => {
	return (
		<DatePicker
			value={stringToDate(value as string)}
			onChange={(date) => onChange?.(formatDate(date))}
			className={`w-full ${className}`}
			placeholder="Chọn ngày"
			format={DISPLAY_DATE_FORMAT}
			{...props}
		/>
	);
};

export default AppDatePicker;
