import { Form, Input } from "antd";
import { RULES } from "~/constants/config";

type Props = {
	passwordName?: string;
	confirmPasswordName?: string;
};

function PasswordField({
	passwordName = "password",
	confirmPasswordName = "confirmPassword",
}: Props) {
	return (
		<>
			<Form.Item label="Mật khẩu" name={passwordName} rules={[RULES.REQUIRED]}>
				<Input.Password placeholder="Nhập mật khẩu" />
			</Form.Item>
			<Form.Item
				label="Xác nhận mật khẩu"
				name={confirmPasswordName}
				dependencies={[passwordName]}
				required
				hasFeedback
				rules={[
					({ getFieldValue }) => ({
						validator(_, value) {
							if (getFieldValue(passwordName) === value) {
								return Promise.resolve();
							}
							return Promise.reject(new Error("Mật khẩu không khớp!"));
						},
					}),
				]}>
				<Input.Password placeholder="Nhập lại mật khẩu" />
			</Form.Item>
		</>
	);
}

export default PasswordField;
