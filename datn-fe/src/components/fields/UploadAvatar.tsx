import { UserOutlined } from "@ant-design/icons";
import { Avatar, GetProp, Upload, UploadProps, message } from "antd";

type FileType = Parameters<GetProp<UploadProps, "beforeUpload">>[0];
export type UploadAvatarValue = {
	url: string;
	file: FileType;
};

export type UploadAvatarProps = {
	onChange?: (value: UploadAvatarValue) => void;
	value?: UploadAvatarValue;
};

const getBase64 = (img: FileType, callback: (url: string) => void) => {
	const reader = new FileReader();
	reader.addEventListener("load", () => {
		const base64Url = reader.result as string;
		callback(base64Url);
	});
	reader.readAsDataURL(img);
};

const beforeUpload = (file: FileType) => {
	const isJpgOrPng = file.type === "image/jpeg" || file.type === "image/png";
	if (!isJpgOrPng) {
		message.error("You can only upload JPG/PNG file!");
	}
	const isLt2M = file.size / 1024 / 1024 < 2;
	if (!isLt2M) {
		message.error("Image must smaller than 2MB!");
	}
	return isJpgOrPng && isLt2M;
};

function UploadAvatar({ onChange, value }: UploadAvatarProps) {
	const handleChange: UploadProps["onChange"] = (info) => {
		if (info.file.status === "uploading") {
			return;
		}
		if (info.file.status === "done") {
			getBase64(info.file.originFileObj as FileType, (url) => {
				onChange?.({
					url,
					file: info.file.originFileObj as FileType,
				});
			});
		}
	};

	return (
		<Upload
			customRequest={({ file, onSuccess }) => {
				onSuccess?.("ok");
			}}
			name="avatar"
			listType="picture-circle"
			className="avatar-uploader"
			showUploadList={false}
			beforeUpload={beforeUpload}
			onChange={handleChange}>
			<Avatar src={value?.url} size={102} icon={<UserOutlined />} />
		</Upload>
	);
}

export default UploadAvatar;
