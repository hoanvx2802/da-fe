import { Form, Input, Select } from "antd";
import { apiUrls } from "~/constants/apiUrl.constants";
import { MODAL_TYPE, RULES, STATUS_OPTIONS } from "~/constants/config";
import { useGetModalType } from "~/stores/modalStore";
import { axiosClient } from "../apis/axiosConfig/axiosClient";
import AppDatePicker from "./fields/AppDatePicker";
import ModalForm from "./form/ModalForm";
import SelectRoom from "./select/SelectRoom";
import SelectStudent from "./select/SelectStudent";

const ModalContract = () => {
  const modalType = useGetModalType();
  return (
    <ModalForm
      title="hợp đồng"
      queryKey="UseContract"
      mutationFn={(values: any) => {
        if (values.id) {
          if (
            !isNaN(values.roomNumber) &&
            typeof values.roomNumber === "number"
          ) {
            values.roomId = parseFloat(values.roomNumber);
          }
          if (!values.studentFullName.includes(" ")) {
            values.studentName = values.studentFullName;
          }
          return axiosClient.put(apiUrls.contract, values);
        } else {
          return axiosClient.post(apiUrls.contract, values);
        }
      }}
    >
      <Form.Item name={"roomNumber"} label="Phòng" rules={[RULES.REQUIRED]}>
        <SelectRoom isContract={true} />
      </Form.Item>
      {(modalType === MODAL_TYPE.DETAIL || modalType === MODAL_TYPE.EDIT) && (
        <>
          <Form.Item name={"building"} label="Tòa nhà" rules={[RULES.REQUIRED]}>
            <Input disabled />
          </Form.Item>
        </>
      )}
      <Form.Item
        name={"studentFullName"}
        label="Sinh viên"
        rules={[RULES.REQUIRED]}
      >
        <SelectStudent isContract={true} />
      </Form.Item>

      <Form.Item label="Ngày bắt đầu" name={"startAt"} rules={[RULES.REQUIRED]}>
        <AppDatePicker />
      </Form.Item>

      <Form.Item label="Ngày kết thúc" name={"endAt"} rules={[RULES.REQUIRED]}>
        <AppDatePicker />
      </Form.Item>
      {(modalType === MODAL_TYPE.DETAIL || modalType === MODAL_TYPE.EDIT) && (
        <>
          <Form.Item label="Người phê duyệt" name={"acceptByFull"}>
            <Input disabled />
          </Form.Item>
          <Form.Item label="Trạng thái" name={"status"}>
            <Select disabled>
              {STATUS_OPTIONS.map((option) => (
                <Select.Option key={option.value} value={option.value}>
                  {option.label}
                </Select.Option>
              ))}
            </Select>
          </Form.Item>
          <Form.Item label="Ngày tạo" name={"createAt"}>
            <AppDatePicker disabled />
          </Form.Item>
          <Form.Item label="Ngày chỉnh sửa" name={"updateAt"}>
            <AppDatePicker disabled />
          </Form.Item>
          <Form.Item label="Người chỉnh sửa" name={"changeFullName"}>
            <Input disabled />
          </Form.Item>
        </>
      )}
    </ModalForm>
  );
};

export default ModalContract;
