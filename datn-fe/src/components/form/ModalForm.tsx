import { useMutation, useQueryClient } from "@tanstack/react-query";
import { Button, Form, FormProps, Modal, ModalProps, message } from "antd";
import { ReactNode, useEffect, useMemo } from "react";
import { MODAL_TYPE } from "~/constants/config";
import { FORM_LAYOUT } from "~/global/theme/config";
import useModalStore from "~/stores/modalStore"; // Update the import path

export interface ModalFormProps extends FormProps {
	mutationFn: (values: any) => Promise<any>;
	convertData?: ((data: any) => any) | ((data: any) => Promise<any>);
	queryKey?: string;
	title?: string;
	children?: ReactNode;
	modalProps?: ModalProps;
}

const ModalForm = ({
	mutationFn,
	queryKey,
	children,
	title = "",
	convertData,
	modalProps,
	...formProps
}: ModalFormProps) => {
	const [form] = Form.useForm();
	const queryClient = useQueryClient();
	const { closeModal, data, isOpen, modalType } = useModalStore(
		(state) => state
	);
	const isDetail = modalType === MODAL_TYPE.DETAIL;

	const modalTitle = useMemo(() => {
		let titleType = "";
		switch (modalType) {
			case MODAL_TYPE.EDIT:
				titleType = "Chỉnh sửa";
				break;
			case MODAL_TYPE.DETAIL:
				titleType = "Chi tiết";
				break;
			default:
				titleType = "Thêm mới";
		}
		return titleType + " " + title;
	}, [modalType]);

	const handleClose = () => {
		closeModal();
		form.resetFields();
	};

	useEffect(() => {
		if (!isOpen || !data) return;
		const initializeData = async () => {
			if (convertData) {
				const convert = await convertData(data);
				form.setFieldsValue(convert);
			} else {
				form.setFieldsValue(data);
			}
		};
		initializeData();
	}, [isOpen, data]);

	const mutation = useMutation({
		mutationFn,
		onSuccess: (data) => {
			queryKey && queryClient.invalidateQueries({ queryKey: [queryKey] });
			message.success(`${modalTitle} thành công!`);
			handleClose();
		},
		onError: (error) => {
			message.error(error.message);
		},
	});

	const handleSubmit = (values: any) => {
		mutation.mutate({ ...data, ...values });
	};

	return (
		<Modal
			title={<div className="mb-5">{modalTitle}</div>}
			open={isOpen}
			{...modalProps}
			onCancel={handleClose}
			footer={
				<Button
					className="btn-primary"
					type="primary"
					onClick={isDetail ? handleClose : form.submit}
					loading={mutation.isPending}>
					{isDetail ? "Đóng" : `Lưu ${title}`}
				</Button>
			}>
			<Form
				form={form}
				disabled={isDetail}
				onFinish={handleSubmit}
				{...FORM_LAYOUT}
				{...formProps}>
				{children}
			</Form>
		</Modal>
	);
};

export default ModalForm;
