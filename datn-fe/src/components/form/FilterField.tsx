import { Col, Form, FormItemProps } from "antd";

function FilterField({ children, ...rest }: FormItemProps) {
	return (
		<Col span={5}>
			<Form.Item noStyle {...rest}>
				{children}
			</Form.Item>
		</Col>
	);
}

export default FilterField;
