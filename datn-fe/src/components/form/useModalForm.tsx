import { QueryKey, useMutation, useQueryClient } from "@tanstack/react-query";
import { Form, message } from "antd";
import { ReactNode, useCallback } from "react";
import { FORM_LAYOUT } from "~/global/theme/config";
import { useModalWithContext } from "~/global/theme/ModalFormProvider";

type UseModalFormParams = {
	queryKey?: QueryKey;
};
type ShowModalParams = {
	title: string;
	content: ReactNode;
	onFinish: (values: any) => Promise<any>;
};

function useModalForm({ queryKey }: UseModalFormParams = {}) {
	const modal = useModalWithContext();
	const queryClient = useQueryClient();
	const [form] = Form.useForm();
	const mutation = useMutation({
		mutationFn: (values: any) => values,
		onSuccess: (msg: string) => {
			message.success(msg + " thành công!");
			queryKey && queryClient.invalidateQueries({ queryKey });
		},
		onError: (error: any) => {
			message.error(error.message || "Có lỗi xảy ra!");
		},
	});

	const showModal = useCallback(
		({ content, title, onFinish }: ShowModalParams) => {
			return modal.confirm({
				title: <div className="mb-5">{title}</div>,
				width: 560,
				icon: null,
				maskClosable: true,
				centered: true,
				content: (
					<Form form={form} {...FORM_LAYOUT}>
						{content}
					</Form>
				),
				onOk: async () => {
					const values = await form.validateFields();
					await mutation.mutateAsync(onFinish(values).then(() => title));
				},
				afterClose: () => {
					form.resetFields();
				},
			});
		},
		[]
	);

	return showModal;
}

export default useModalForm;
