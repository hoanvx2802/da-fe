import { ReloadOutlined, SearchOutlined } from "@ant-design/icons";
import { Button, Col, Form, Row } from "antd";
import React, { useEffect } from "react";
import useFilter from "~/hooks/useFilter";

interface FilterFormProps {
	children?: React.ReactNode;
}

function FilterForm({ children }: FilterFormProps) {
	const [form] = Form.useForm();
	const [filter, setFilter] = useFilter();

	useEffect(() => {
		form.setFieldsValue(filter);
	}, []);

	return (
		<Form
			form={form}
			className="mb-5 mt-6"
			onFinish={(values) => setFilter(values)}>
			<Row gutter={16}>
				{children}
				<Col span={4}>
					<Button
						icon={<ReloadOutlined />}
						onClick={() => {
							setFilter({}, false);
							form.resetFields();
						}}
						className="mr-4"
					/>

					<Button type="primary" icon={<SearchOutlined />} htmlType="submit" />
				</Col>
			</Row>
		</Form>
	);
}

export default FilterForm;
