import { Form, Input, Select } from "antd";
import { apiUrls } from "~/constants/apiUrl.constants";
import { RULES, STATUS_OPTIONS } from "~/constants/config";
import { axiosClient } from "../apis/axiosConfig/axiosClient";
import ModalForm from "./form/ModalForm";
import SelectAcceptBy from "./select/SelectAcceptBy";
import { useGetModalType } from "~/stores/modalStore";
import AppDatePicker from "./fields/AppDatePicker";

const ModalRequest = () => {
	const modalType = useGetModalType();
	return (
		<>
			<ModalForm
				mutationFn={(values: any) => {
					return axiosClient.post<Request>(apiUrls.request, values);
				}}
				title="yêu cầu"
				layout="vertical"
				queryKey="UseRequest">
				<SelectAcceptBy />
				<Form.Item
					labelCol={{
						span: 24,
					}}
					wrapperCol={{
						span: 24,
					}}
					label="Nội dung yêu cầu"
					name="description"
					rules={[RULES.REQUIRED]}>
					<Input.TextArea rows={4} />
				</Form.Item>
				{modalType && (
					<>
						<Form.Item label="Ngày tạo" name="createAt">
							<AppDatePicker disabled />
						</Form.Item>
						<Form.Item label="Người tạo" name="createFullName">
							<Input disabled />
						</Form.Item>
						<Form.Item label="Trạng thái" name="status">
							<Select disabled>
									{STATUS_OPTIONS.map((option) => (
										<Select.Option key={option.value} value={option.value}>
											{option.label}
										</Select.Option>
									))}
								</Select>
						</Form.Item>
					</>
				)}
			</ModalForm>
		</>
	);
};

export default ModalRequest;
