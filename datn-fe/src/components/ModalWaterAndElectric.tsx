import { Form, Input } from "antd";

import { apiUrls } from "~/constants/apiUrl.constants";
import { RULES } from "~/constants/config";
import { axiosClient } from "../apis/axiosConfig/axiosClient";
import ModalForm from "./form/ModalForm";

const ModalWaterAndElectric = () => {
  return (
    <>
      <ModalForm
        title="điện nước"
        mutationFn={(values: any) => {
          return axiosClient.put(apiUrls.waterAndElectric, values);
        }}
        queryKey="UseWaterAndElectric"
      >
        <Form.Item label="Tên phòng" rules={[RULES.REQUIRED]} name={"number"}>
          <Input disabled={true} />
        </Form.Item>
        <Form.Item
          label="Mã công tơ nước"
          rules={[RULES.REQUIRED]}
          name={"waterCode"}
        >
          <Input placeholder="Mã công tơ nước" />
        </Form.Item>
        <Form.Item
          label="Chỉ số nước cũ"
          rules={[RULES.REQUIRED]}
          name={"startWaterNumber"}
        >
          <Input />
        </Form.Item>
        <Form.Item
          label="Chỉ số nước mới"
          rules={[RULES.REQUIRED]}
          name={"endWaterNumber"}
        >
          <Input />
        </Form.Item>
        <Form.Item
          label="Mã công tơ điện"
          rules={[RULES.REQUIRED]}
          name={"electricMeterCode"}
        >
          <Input placeholder="Mã công tơ nước" />
        </Form.Item>
        <Form.Item
          label="Chỉ số điện cũ"
          rules={[RULES.REQUIRED]}
          name={"startElectricNumber"}
        >
          <Input />
        </Form.Item>
        <Form.Item
          label="Chỉ số điện mới"
          rules={[RULES.REQUIRED]}
          name={"endElectricNumber"}
        >
          <Input />
        </Form.Item>
      </ModalForm>
    </>
  );
};

export default ModalWaterAndElectric;
