import { Select } from "antd";
import { CONTRACT_STATUS_OPTIONS } from "~/constants/config";
import AppRangePicker from "../fields/AppRangePicker";
import FilterField from "../form/FilterField";
import FilterWithSearch from "./FilterWithSearch";

const FilterManagerWithTimeRange = () => {
	return (
		<FilterWithSearch
			placeholder="Tìm yêu cầu"
			children={
				<>
					<FilterField name="range">
						<AppRangePicker />
					</FilterField>
					<FilterField name="status">
						<Select
							className="w-full"
							placeholder="Chọn trạng thái"
							allowClear
							mode="multiple"
							options={CONTRACT_STATUS_OPTIONS as any}
						/>
					</FilterField>
				</>
			}
		/>
	);
};

export default FilterManagerWithTimeRange;
