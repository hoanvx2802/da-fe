import { Input } from "antd";
import FilterField from "../form/FilterField";
import FilterForm from "../form/FilterForm";
import { ReactNode } from "react";

const FilterWithSearch = ({ children, placeholder="Nhập từ khóa" }: { children?: ReactNode, placeholder?: string }) => {
	return (
		<FilterForm>
			{children}
			<FilterField name="keyword">
				<Input placeholder={placeholder} allowClear />
			</FilterField>
		</FilterForm>
	);
};

export default FilterWithSearch;
