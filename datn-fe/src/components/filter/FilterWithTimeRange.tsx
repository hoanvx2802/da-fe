import { ReactNode } from "react";
import AppRangePicker from "../fields/AppRangePicker";
import FilterField from "../form/FilterField";
import FilterWithSearch from "./FilterWithSearch";


const FilterWithTimeRange = ({ children, placeholder="Nhập từ khóa" }: { children?: ReactNode, placeholder?: string }) => {
	return (
		<FilterWithSearch
			placeholder={placeholder ? placeholder : "Tìm từ khóa"}
			children={
				<>
					{children}
					<FilterField name="range">
						<AppRangePicker />
					</FilterField>
				</>
			}
		/>
	);
};

export default FilterWithTimeRange;
