import { Select } from "antd";
import React, { useEffect, useState } from "react";
import FilterField from "../form/FilterField";
import FilterForm from "../form/FilterForm";
import { ROOM_STATUS_OPTIONS } from "~/constants/config";

const { Option } = Select;

interface RoomData {
	building: string;
	number: string;
	status: string;
}

interface FilterRoomProps {
	data: RoomData[];
}

const FilterRoom: React.FC<FilterRoomProps> = ({ data }) => {
	const [buildings, setBuildings] = useState<string[]>([]);
	const [roomNumbers, setRoomNumbers] = useState<string[]>([]);

	useEffect(() => {
		if (data) {
			const uniqueBuildings = Array.from(
				new Set(data.map((item) => item.building))
			);
			const uniqueRoomNumbers = Array.from(
				new Set(data.map((item) => item.number))
			);

			setBuildings(uniqueBuildings);
			setRoomNumbers(uniqueRoomNumbers);
		}
	}, [data]);

	return (
		<FilterForm>
			<FilterField name="building">
				<Select className="w-full" placeholder="Chọn tòa nhà" allowClear>
					{buildings.map((building) => (
						<Option key={building} value={building}>
							{building}
						</Option>
					))}
				</Select>
			</FilterField>
			<FilterField name="roomNumber">
				<Select className="w-full" placeholder="Chọn số phòng" allowClear>
					{roomNumbers.map((roomNumber) => (
						<Option key={roomNumber} value={roomNumber}>
							{roomNumber}
						</Option>
					))}
				</Select>
			</FilterField>
			<FilterField name="status">
				<Select
					className="w-full"
					mode="multiple"
					placeholder="Chọn trạng thái phòng"
					allowClear
					options={ROOM_STATUS_OPTIONS as any}
				/>
			</FilterField>
		</FilterForm>
	);
};

export default FilterRoom;
