import { Select, SelectProps } from "antd";

import useStudent from "~/pages/Students/hook/useStudents";
interface SelectStudentProps extends SelectProps {
	isContract: boolean;
  }
function SelectStudent({isContract, ...props}: SelectStudentProps) {
	const { data, isLoading } = useStudent({
	    isContract: isContract
    });
	const requestData = data || [];
	const dataX = requestData.data || [];

	const options =
		dataX.map((student: any) => ({
			value: student?.username,
			label: student?.fullName,
		})) || [];

	return (
		<Select
			options={options}
			className="w-full"
			placeholder="Chọn sinh viên"
			loading={isLoading}
			{...props}
		/>
	);
}

export default SelectStudent;
