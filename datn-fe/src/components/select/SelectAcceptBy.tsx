import { useQuery } from "@tanstack/react-query";
import { Form, Select } from "antd";
import { ReactNode, useMemo } from "react";
import { axiosClient } from "~/apis/axiosConfig/axiosClient";
import { apiUrls } from "~/constants/apiUrl.constants";

type Props = {
	name?: string;
	label?: ReactNode;
};

function SelectAcceptBy({
	name = "acceptBy",
	label = "Người xác nhận",
}: Props) {
	const { data, isLoading } = useQuery({
		queryKey: ["UseAcceptBy"],
		queryFn: () =>
			axiosClient.get(apiUrls.employeeAccept).then((res) => res.data),
	});

	const options = useMemo(() => {
		return data?.map((acceptUsers: any) => ({
			value: acceptUsers?.username,
			label: acceptUsers?.fullName,
		}));
	}, [data]);
	return (
		<Form.Item name={name} label={label}>
			<Select
				className="w-full"
				placeholder="Chọn người xác nhận"
				options={options}
				loading={isLoading}
			/>
		</Form.Item>
	);
}

export default SelectAcceptBy;
