import { Select, SelectProps } from "antd";
import { ROOM_STATUS } from "~/constants/config";

import useRooms from "~/pages/Room/hook/useRoom";
interface SelectRoomProps extends SelectProps {
  isContract: boolean;
}
function SelectRoom({isContract, ...props}: SelectRoomProps) {
	const { data, isLoading } = useRooms({
		status: [ROOM_STATUS.EMPTY, ROOM_STATUS.NOT_FULL],
    	isContract: isContract
    });

	const requestData = data || [];
	const dataX = requestData.data || [];

	const options =
		dataX.map((room: any) => ({
			value: room?.id,
			label: room?.number + " tòa " + room?.building,
		})) || [];

	return (
		<Select
			options={options}
			className="w-full"
			placeholder="Chọn phòng"
			loading={isLoading}
			{...props}
		/>
	);
}

export default SelectRoom;
