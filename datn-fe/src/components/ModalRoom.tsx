import { Checkbox, Collapse, Form, Input, InputNumber, Radio } from "antd";
import { apiUrls } from "~/constants/apiUrl.constants";
import { axiosClient } from "../apis/axiosConfig/axiosClient";
import ModalForm from "./form/ModalForm";
import { MODAL_TYPE, RULES } from "~/constants/config";
import { useGetModalType } from "~/stores/modalStore";
const { Panel } = Collapse;
const ModalRoom = () => {
  const modalType = useGetModalType();
  return (
    <ModalForm
      title="phòng"
      queryKey="UseRoom"
      mutationFn={(values: any) => {
        if (values.id) {
          return axiosClient.put(apiUrls.room, values);
        } else {
          return axiosClient.post(apiUrls.room, values);
        }
      }}
    >
      <Form.Item label="Tên phòng" rules={[RULES.REQUIRED]} name={"number"}>
        <Input placeholder="Tên phòng hoặc số phòng" />
      </Form.Item>
      <Form.Item label="Tòa nhà" rules={[RULES.REQUIRED]} name={"building"}>
        <Input placeholder="Nhập tên tòa nhà" />
      </Form.Item>
      <Form.Item
        label="Số lượng tối đa"
        rules={[RULES.REQUIRED]}
        name={"capacity"}
      >
        <InputNumber placeholder="Số người tối đa" min={1} className="w-full" />
      </Form.Item>
      {(modalType === MODAL_TYPE.EDIT || modalType === MODAL_TYPE.DETAIL) && (
        <>
          <Form.Item label="Số lượng hiện có" name={"currentQuantity"}>
            <Input
              disabled={true}
              placeholder="Số người tối đa"
              className="w-full"
            />
          </Form.Item>
        </>
      )}
      <Form.Item label="Giá phòng" rules={[RULES.REQUIRED]} name={"price"}>
        <InputNumber
          prefix="VNĐ"
          placeholder="Nhập giá phòng"
          className="w-full"
        />
      </Form.Item>
      <Form.Item
        label="Sẵn sàng để ở"
        rules={[RULES.REQUIRED]}
        name="available"
      >
        <Radio.Group>
          <Radio value={true}>Đã sẵn sàng</Radio>
          <Radio value={false}>Chưa sẵn sàng</Radio>
        </Radio.Group>
      </Form.Item>
      <Form.Item
        labelCol={{
          span: 24,
        }}
        wrapperCol={{
          span: 24,
        }}
        label="Mô tả"
        name="description"
      >
        <Input.TextArea rows={4} />
      </Form.Item>
      <Collapse>
        <Panel header="Cơ sở vật chất" key="1">
          <Form.Item name="isAirConditioner" valuePropName="checked">
            <Checkbox>Điều hòa</Checkbox>
          </Form.Item>
          <Form.Item name="isFridge" valuePropName="checked">
            <Checkbox>Tủ lạnh</Checkbox>
          </Form.Item>
          <Form.Item name="isStudyDesk" valuePropName="checked">
            <Checkbox>Bàn học cá nhân</Checkbox>
          </Form.Item>
          <Form.Item name="isWardrobe" valuePropName="checked">
            <Checkbox>Tủ quần áo</Checkbox>
          </Form.Item>
          <Form.Item name="isWashingMachine" valuePropName="checked">
            <Checkbox>Máy giặt</Checkbox>
          </Form.Item>
          <Form.Item name="isWaterHeater" valuePropName="checked">
            <Checkbox>Bình nóng lạnh</Checkbox>
          </Form.Item>
        </Panel>
      </Collapse>
    </ModalForm>
  );
};

export default ModalRoom;
