import {
	DeleteOutlined,
	EditOutlined,
	EllipsisOutlined,
	InfoCircleOutlined,
} from "@ant-design/icons";
import { Button, Dropdown } from "antd";
import { useMemo } from "react";

type Props = {};

function TableActions({}: Props) {
	const items = useMemo(() => {
		return [
			{
				key: "1",
				className: "w-[180px]",
				label: "Chỉnh sửa",
				icon: <EditOutlined />,
			},
			{
				key: "2",
				label: "Chi tiết",
				icon: <InfoCircleOutlined />,
			},
			{
				key: "3",
				label: "Xóa",
				danger: true,
				icon: <DeleteOutlined />,
			},
		];
	}, []);
	return (
		<Dropdown menu={{ items }} placement="bottomRight" className="text-center">
			<Button type="text" icon={<EllipsisOutlined />} />
		</Dropdown>
	);
}

export default TableActions;
