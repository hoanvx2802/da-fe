import { Table, TableProps } from "antd";
import React from "react";
import { useQueryParams } from "~/hooks/useFilter";

interface Props extends TableProps {}

const TableData: React.FC<Props> = ({ columns, dataSource }) => {
	const [filter, setFilter] = useQueryParams();

	const handlePage = (currPage: number, pageSize: number) => {
		setFilter({ page: currPage, limit: pageSize });
	};

	return (
		<>
			<Table
				columns={columns}
				dataSource={dataSource}
				pagination={{
					showQuickJumper: true,
					showSizeChanger: true,
					pageSize: filter.limit,
					current: filter.page,
					onChange: (page, pageSize) => handlePage(page, pageSize),
				}}
				scroll={{ x: "max-content", y: 340 }}
			/>
		</>
	);
};

export default TableData;
