import { TableProps } from "antd";
import { FullScreen, useFullScreenHandle } from "react-full-screen";
import TableData from "./TableData";
import TableTitle from "./TableTitle";

interface Props<Record> extends TableProps<Record> {
	totalCol: TotalColType<Record>;
	setTotalCol: React.Dispatch<React.SetStateAction<TotalColType<Record>>>;
	title: any;
}

type TotalColType<Record> = NonNullable<Props<Record>["columns"]>;

const TableContent = <T extends object>({
	columns,
	dataSource,
	totalCol,
	setTotalCol,
	title,
}: Props<T>) => {
	const handle = useFullScreenHandle();

	return (
		<FullScreen className="bg-white" handle={handle}>
			<TableTitle
				handle={handle}
				columns={columns}
				title={title}
				totalCol={totalCol}
				setTotalCol={setTotalCol}
			/>
			<TableData
				columns={totalCol.filter((col: any) => !col.hidden)}
				dataSource={dataSource}
			/>
		</FullScreen>
	);
};

export default TableContent;
