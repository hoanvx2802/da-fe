import {
  EyeOutlined,
  FullscreenExitOutlined,
  FullscreenOutlined,
} from "@ant-design/icons";
import { Checkbox, Dropdown, MenuProps } from "antd";
import { useState } from "react";
import { FullScreenHandle } from "react-full-screen";

interface Props {
  handle: FullScreenHandle;
  columns: any;
  title: string;
  totalCol: any;
  setTotalCol: React.Dispatch<any>;
}

const TableTitle: React.FC<Props> = ({
  handle,
  columns,
  title,
  totalCol,
  setTotalCol,
}) => {
  const [visibleCols, setVisibleCols] = useState<string[]>(
    totalCol.map((col: any) => col.key)
  );

  function sortColumnByKey(colKey: string) {
    const updatedCols = visibleCols.includes(colKey)
      ? visibleCols.filter((key) => key !== colKey)
      : [...visibleCols, colKey];

    setVisibleCols(updatedCols);

    setTotalCol((prev: any) =>
      prev.map((col: any) => ({
        ...col,
        hidden: !updatedCols.includes(col.key),
      }))
    );
  }

  const items: MenuProps["items"] = columns.map((col: any) => ({
    label: (
      <Checkbox
        checked={visibleCols.includes(col.key)}
        onChange={() => sortColumnByKey(col.key)}
      >
        {col.title}
      </Checkbox>
    ),
    key: col.key,
  }));

  return (
    <>
      <div className="flex items-center justify-between py-2 px-2">
        <p className="font-medium text-gray-900">{title}</p>
        <div>
          <Dropdown menu={{ items }} trigger={["click"]}>
            <EyeOutlined className="pr-2 text-lg" />
          </Dropdown>
          {handle.active ? (
            <FullscreenExitOutlined className="text-lg" onClick={handle.exit} />
          ) : (
            <FullscreenOutlined className="text-lg" onClick={handle.enter} />
          )}
        </div>
      </div>
    </>
  );
};

export default TableTitle;
