import { Form, Input } from "antd";
import { useCallback } from "react";
import { axiosClient } from "~/apis/axiosConfig/axiosClient";
import { apiUrls } from "~/constants/apiUrl.constants";
import PasswordField from "../fields/PasswordField";
import useModalForm from "../form/useModalForm";

const useChangePasswordModal = () => {
	const showPasswordModal = useModalForm();

	const showModal = useCallback(async () => {
		return showPasswordModal({
			title: "Đổi mật khẩu",
			content: (
				<>
					<Form.Item
						label="Mật khẩu cũ"
						name="oldPassword"
						rules={[
							{
								required: true,
							},
						]}>
						<Input.Password />
					</Form.Item>
					<PasswordField passwordName="newPassword" />
				</>
			),
			onFinish: (values) => axiosClient.post(apiUrls.changePassword, values),
		});
	}, []);

	return showModal;
};

export default useChangePasswordModal;
