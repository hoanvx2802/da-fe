import { Button, Form, Input, Modal, ModalProps, message } from "antd";
import { useCallback, useEffect } from "react";
import useGetUser, { useUpdateUser } from "~/hooks/useGetUser";
import UploadAvatar from "../fields/UploadAvatar";
import useChangePasswordModal from "./ChangePasswordModal";
import { FORM_LAYOUT } from "~/global/theme/config";
import { RULES } from "~/constants/config";

interface ModalUserInfoProps extends ModalProps {
	onCancel?: () => void;
}

function ModalUserInfo({ onCancel, open, ...other }: ModalUserInfoProps) {
	const [form] = Form.useForm();
	const user = useGetUser();
	const showModal = useChangePasswordModal();

	useEffect(() => {
		if (open && user && Object.keys(user).length > 0) {
			form.setFieldsValue(user);
		}
	}, [user, open]);

	const onUpdateSuccess = useCallback(() => {
		message.success("Cập nhật thông tin thành công");
		onCancel?.();
	}, [onCancel]);

	const mutation = useUpdateUser(onUpdateSuccess);

	return (
		<Modal
			title={<div className="mb-5">Thông tin cá nhân</div>}
			open={open}
			onCancel={onCancel}
			footer={
				<>
					<Button onClick={showModal}>Đổi mật khẩu</Button>
					<Button
						type="primary"
						onClick={form.submit}
						loading={mutation.isPending}>
						Lưu
					</Button>
				</>
			}>
			<Form form={form} onFinish={mutation.mutate} {...FORM_LAYOUT}>
				<Form.Item name="avatar">
					<UploadAvatar />
				</Form.Item>
				<Form.Item label="Email" name="email" rules={[RULES.EMAIL]}>
					<Input />
				</Form.Item>
				<Form.Item label="Tên đăng nhập" name="username">
					<Input disabled/>
				</Form.Item>
				<Form.Item label="Họ và tên" name="fullName">
					<Input />
				</Form.Item>
				<Form.Item label="Số điện thoại" name="phoneNumber">
					<Input />
				</Form.Item>
				<Form.Item label="CCCD" name="citizenIdentificationCard">
					<Input />
				</Form.Item>
				<Form.Item label="Địa chỉ" name="address">
					<Input />
				</Form.Item>
			</Form>
		</Modal>
	);
}

export default ModalUserInfo;
