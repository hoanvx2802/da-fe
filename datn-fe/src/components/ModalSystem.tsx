import { Form, Input } from "antd";
import { apiUrls } from "~/constants/apiUrl.constants";
import { axiosClient } from "../apis/axiosConfig/axiosClient";
import ModalForm from "./form/ModalForm";
import { RULES } from "~/constants/config";

const ModalSystem = () => {
  return (
    <>
      <ModalForm
        mutationFn={(values: any) => {
          if (values.id) {
            return axiosClient.put(apiUrls.system, values);
          } else {
            return axiosClient.post(apiUrls.system, values);
          }
        }}
        title="cấu hình"
        layout="vertical"
        queryKey="UseSystem"
      >
        <Form.Item label="Khóa" name="keyword" rules={[RULES.REQUIRED]}>
          <Input />
        </Form.Item>
        <Form.Item label="Giá trị" name="value" rules={[RULES.REQUIRED]}>
          <Input />
        </Form.Item>
      </ModalForm>
    </>
  );
};

export default ModalSystem;
