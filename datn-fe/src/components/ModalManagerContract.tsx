import { Form, Input } from "antd";
import { apiUrls } from "~/constants/apiUrl.constants";
import { RULES } from "~/constants/config";
import { axiosClient } from "../apis/axiosConfig/axiosClient";
import AppDatePicker from "./fields/AppDatePicker";
import ModalForm from "./form/ModalForm";
import SelectRoom from "./select/SelectRoom";
import SelectStudent from "./select/SelectStudent";

const ModalManagerContract = () => {
  return (
    <ModalForm
      title="hợp đồng"
      queryKey="UseContract"
      mutationFn={(values: any) => {
        if (values.id) {
          return axiosClient.put(apiUrls.contract, values);
        } else {
          return axiosClient.post(apiUrls.contract, values);
        }
      }}
    >
      <Form.Item name={"roomNumber"} label="Phòng" rules={[RULES.REQUIRED]}>
        <SelectRoom isContract={true} />
      </Form.Item>
      <Form.Item name={"building"} label="Tòa nhà" rules={[RULES.REQUIRED]}>
        <Input disabled/>
      </Form.Item>
      <Form.Item
        name={"studentFullName"}
        label="Sinh viên"
        rules={[RULES.REQUIRED]}
      >
        <SelectStudent isContract={true} />
      </Form.Item>

      <Form.Item
        label="Ngày bắt đầu"
        name={"startAt"}
        rules={[RULES.REQUIRED]}
      >
        <AppDatePicker />
      </Form.Item>

      <Form.Item
        label="Ngày kết thúc"
        name={"endAt"}
        rules={[RULES.REQUIRED]}
      >
        <AppDatePicker />
      </Form.Item>
      <Form.Item label="Ngày tạo" name={"createAt"}>
        <AppDatePicker disabled />
      </Form.Item>
      <Form.Item label="Người phê duyệt" name={"acceptByFull"}>
        <Input disabled />
      </Form.Item>
      <Form.Item label="Ngày chỉnh sửa" name={"updateAt"}>
        <AppDatePicker disabled />
      </Form.Item>
      <Form.Item label="Người chỉnh sửa" name={"changeNameFull"}>
        <Input disabled />
      </Form.Item>
    </ModalForm>
  );
};

export default ModalManagerContract;
