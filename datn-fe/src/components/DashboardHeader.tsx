import { UserOutlined } from "@ant-design/icons";
import { useQueryClient } from "@tanstack/react-query";
import { Avatar } from "antd";
import React, { useState } from "react";
import { Link } from "react-router-dom";
import useGetUser from "~/hooks/useGetUser";
import HUCE from "../assets/HUCE.png";
import ModalUserInfo from "./ModalUserInfo/ModalUserInfo";

interface Props {
	keyValue: string;
}

const DashboardHeader: React.FC<Props> = ({ keyValue }) => {
	const [open, setOpen] = useState(false);
	const queryClient = useQueryClient();
	const user = useGetUser();
	return (
		<div className="w-full h-full flex items-center">
			<div className="flex items-center gap-4 mr-[70px] ml-[50px]">
				<div className="flex items-center justify-center">
					<img className="w-10 h-10 pr-1" src={HUCE} alt="hivetech-logo" />
					<div className="flex flex-col text-center">
						<p className="text-black text-lg font-bold">HUCE</p>
					</div>
				</div>
			</div>
			<div className="flex items-center justify-between">
				<div>
					QLKTX / <span className="text-blue-500">{keyValue}</span>
				</div>
			</div>
			<div className="ml-auto flex items-center gap-3">
				<div
					className="flex items-center gap-2 cursor-pointer"
					onClick={() => setOpen(true)}>
					<Avatar size={28} icon={<UserOutlined />} src={user.avatar?.url} />
					<p>{user.fullName}</p>
				</div>
				<Link
					to="/login"
					onClick={() => {
						localStorage.clear();
						queryClient.clear();
					}}>
					Logout
				</Link>
			</div>
			<ModalUserInfo open={open} onCancel={() => setOpen(false)} />
		</div>
	);
};

export default DashboardHeader;
