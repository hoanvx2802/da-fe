import { Button, Modal, ModalProps } from "antd";
import React from "react";

interface Props extends ModalProps {
	handleBack: () => void;
}

const ModalComfirm: React.FC<Props> = (props) => {
	const { handleBack, onCancel, open } = props;
	return (
		<Modal
			title="Xác nhận"
			open={open}
			onCancel={handleBack}
			width={700}
			footer={
				<Button className="btn-primary" type="primary" onClick={onCancel}>
					Xác nhận
				</Button>
			}>
			<div>Bạn có chắc muốn thoát yêu cầu này không?</div>
		</Modal>
	);
};

export default ModalComfirm;
