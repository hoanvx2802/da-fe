import { Collapse, Form, Input, Radio } from "antd";
import { revertData } from "~/apis/upload";
import { apiUrls } from "~/constants/apiUrl.constants";
import { MODAL_TYPE, RULES } from "~/constants/config";
import { useTransformImageDefault } from "~/hooks/useTransformImage";
import { useGetModalType } from "~/stores/modalStore";
import { axiosClient } from "../apis/axiosConfig/axiosClient";
import PasswordField from "./fields/PasswordField";
import UploadAvatar from "./fields/UploadAvatar";
import ModalForm from "./form/ModalForm";
const { Panel } = Collapse;
const ModalStudent = () => {
  const modalType = useGetModalType();
  const convertGetData = useTransformImageDefault();
  return (
    <ModalForm
      title="sinh viên"
      convertData={convertGetData}
      mutationFn={async (values: any) => {
        const student = await revertData(values, "imageUrl", "avatar");
        if (student.id) {
          return axiosClient.put(apiUrls.student, student);
        } else {
          return axiosClient.post(apiUrls.student, student);
        }
      }}
      queryKey="UseStudent"
    >
      <Form.Item name="avatar">
        <UploadAvatar />
      </Form.Item>
      {(modalType === MODAL_TYPE.DETAIL || modalType === MODAL_TYPE.EDIT) && (
        <>
          <Form.Item
            label="Tên đăng nhập"
            rules={[RULES.REQUIRED]}
            name={"username"}
          >
            <Input disabled={true} />
          </Form.Item>
        </>
      )}
      {modalType === MODAL_TYPE.ADD && <PasswordField />}
      <Form.Item label="Họ và tên" rules={[RULES.REQUIRED]} name={"fullName"}>
        <Input placeholder="Nhập họ và tên" />
      </Form.Item>
      <Form.Item label="email" rules={[RULES.EMAIL]} name={"email"}>
        <Input placeholder="Nhập email" />
      </Form.Item>
      <Form.Item label="Giới tính" name={"sex"}>
        <Radio.Group>
          <Radio value={true}>Nam</Radio>
          <Radio value={false}>Nữ</Radio>
        </Radio.Group>
      </Form.Item>
      <Form.Item label="Số điện thoại" name={"phoneNumber"}>
        <Input placeholder="Nhập số điện thoại" />
      </Form.Item>
      <Form.Item
        labelCol={{
          span: 24,
        }}
        wrapperCol={{
          span: 24,
        }}
        label="Địa chỉ"
        name="address"
      >
        <Input.TextArea rows={4} />
      </Form.Item>
      {(modalType === MODAL_TYPE.DETAIL || modalType === MODAL_TYPE.EDIT) && (
        <>
          <Form.Item label="Phòng" name={"roomNumber"}>
            <Input disabled placeholder="Chưa có phòng"/>
          </Form.Item>
        </>
      )}

      <Collapse>
        <Panel header="Thông tin lớp học" key="1">
          <Form.Item
            label="Mã sinh viên"
            rules={[RULES.REQUIRED]}
            name={"studentCode"}
          >
            <Input placeholder="" />
          </Form.Item>
          <Form.Item label="Lớp" rules={[RULES.REQUIRED]} name={"className"}>
            <Input placeholder="" />
          </Form.Item>
          <Form.Item label="Khoa" rules={[RULES.REQUIRED]} name={"department"}>
            <Input placeholder="" />
          </Form.Item>
        </Panel>
      </Collapse>
      <div style={{ marginTop: "16px" }}>
        <Form.Item label="Sđt khẩn cấp" name={"emergencyNumber"}>
          <Input placeholder="" />
        </Form.Item>
      </div>
    </ModalForm>
  );
};

export default ModalStudent;
