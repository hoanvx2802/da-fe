import { create } from "zustand";

type ModalStore = {
	isOpen: boolean;
	modalType?: string;
	data: any;
	openModal: (data?: any, modalType?: string) => void;
	closeModal: () => void;
};

const useModalStore = create<ModalStore>((set) => ({
	isOpen: false,
	data: null,
	openModal: (data?: any, modalType?: string) =>
		set({ isOpen: true, data, modalType }),
	closeModal: () => set({ isOpen: false, data: null }),
}));

export const useOpenModal = () => useModalStore((state) => state.openModal);
export const useGetModalType = () => useModalStore((state) => state.modalType);

export default useModalStore;
