import dayjs from "dayjs";
import { create } from "zustand";

type Store = {
  month: number;
  year: number;
  updateDate: (montVal: number, yearVal: number) => void;
};

const useDateStore = create<Store>()((set) => ({
  month: dayjs(new Date()).month(),
  year: dayjs(new Date()).year(),
  updateDate: (monthVal: number, yearVal: number) =>
    set((state) => ({
      ...state,
      month: monthVal,
      year: yearVal,
    })),
}));

export default useDateStore;
