import { EllipsisOutlined } from "@ant-design/icons";
import { useMutation, useQueryClient } from "@tanstack/react-query";
import { Button, Dropdown, message } from "antd";
import { ItemType } from "antd/es/menu/hooks/useItems";
import { useCallback, useMemo } from "react";
import { axiosClient } from "~/apis/axiosConfig/axiosClient";
import {
	DEFAULT_TABLE_ACTIONS,
	TABLE_ACTION_ITEMS,
	TABLE_ACTIONS,
} from "~/constants/config";
import { useOpenModal } from "~/stores/modalStore";
import showConfirm from "~/utils/showConfirm";

export type UseActionColParams = {
	queryKey?: string;
	api: string;
	onSelect?: (key: string, record: any) => void;
	items?: ItemType[] | ((record: any) => ItemType[]);
};

function useActionCol({
	api,
	queryKey,
	onSelect,
	items = DEFAULT_TABLE_ACTIONS as any,
}: UseActionColParams) {
	const queryClient = useQueryClient();
	const openModal = useOpenModal();
	const mutation = useMutation({
		mutationFn: (values: any) => {
			return axiosClient.delete<Request>(api + "/" + values.id);
		},
		onSuccess: (data) => {
			message.success("Xóa thành công");
			queryKey && queryClient.invalidateQueries({ queryKey: [queryKey] });
		},
		onError: (error) => {
			message.error(error.message);
		},
	});

	const handleDelete = useCallback(
		(record: any) => {
			showConfirm({
				title: "Xác nhận xóa",
				content: "Bạn có chắc chắn muốn xóa bản ghi này?",
				onOk: () => mutation.mutate(record),
				okButtonProps: { danger: true },
			});
		},
		[mutation.mutate]
	);

	const handleSelect = useCallback(
		(key: string, record: any) => {
			switch (key) {
				case TABLE_ACTIONS.EDIT:
				case TABLE_ACTIONS.DETAIL:
					openModal(record, key);
					break;
				case TABLE_ACTIONS.DELETE:
					handleDelete(record);
					break;
				default:
					onSelect && onSelect(key, record);
					break;
			}
		},
		[openModal, handleDelete]
	);

	return useMemo(() => {
		return {
			title: "Hành động",
			key: "action",
			width: 80,
			render: (_: any, record: any) => {
				const actionItems = typeof items === "function" ? items(record) : items;
				return (
					<Dropdown
						menu={{
							items: [...actionItems],
							onClick: ({ key }) => handleSelect(key, record),
						}}
						trigger={["click"]}
						placement="bottomRight"
						className="text-center">
						<Button type="text" icon={<EllipsisOutlined />} />
					</Dropdown>
				);
			},
		};
	}, [handleSelect, items]);
}

export default useActionCol;
