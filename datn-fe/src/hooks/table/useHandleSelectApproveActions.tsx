import { QueryKey } from "@tanstack/react-query";
import { Form, Input } from "antd";
import { useCallback } from "react";
import { axiosClient } from "~/apis/axiosConfig/axiosClient";
import useModalForm from "~/components/form/useModalForm";
import { RULES, TABLE_ACTION_APPROVE_LABELS } from "~/constants/config";

function useHandleSelectApproveActions(api: string, queryKey?: QueryKey) {
	const showModal = useModalForm({ queryKey });
	return useCallback(
		(key: string, record: any) => {
			let title =
				TABLE_ACTION_APPROVE_LABELS[
					key as keyof typeof TABLE_ACTION_APPROVE_LABELS
				];
			title &&
				showModal({
					title: title + " yêu cầu",
					content: (
						<Form.Item
							labelCol={{
								span: 24,
							}}
							wrapperCol={{
								span: 24,
							}}
							label="Nhận xét"
							name="comment"
							rules={[RULES.REQUIRED]}>
							<Input.TextArea rows={3} />
						</Form.Item>
					),
					onFinish: (values) => {
						return axiosClient.post(api, {
							...values,
							id: record.id,
							status: key,
						});
					},
				});
		},
		[showModal]
	);
}

export default useHandleSelectApproveActions;
