import { useMutation, useQuery, useQueryClient } from "@tanstack/react-query";
import { message } from "antd";
import { useMemo } from "react";
import { useNavigate } from "react-router-dom";
import { axiosClient } from "~/apis/axiosConfig/axiosClient";
import {
	convertGetData,
	getImage,
	revertData,
	uploadImage,
} from "~/apis/upload";
import { UploadAvatarValue } from "~/components/fields/UploadAvatar";
import { apiUrls } from "~/constants/apiUrl.constants";
import { authKeys } from "~/global/keys/authKeys";

type User = {
	active: boolean;
	address?: string;
	citizenIdentificationCard?: string;
	email: string;
	fullName?: string;
	gender?: string;
	id: number;
	imageUrl?: string;
	phoneNumber?: string;
	role: string;
	username: string;
	avatar: UploadAvatarValue;
};

const USER_KEY = "current-user";

const useGetUser = () => {
	const username = localStorage.getItem(authKeys.username);
	const navigate = useNavigate();
	const { data, isError } = useQuery({
		queryKey: [USER_KEY, username],
		queryFn: async () => {
			const { data } = await axiosClient.get(`${apiUrls.user}/${username}`);
			return convertGetData(data, "imageUrl", "avatar") as Promise<User>;
		},
	});

	if (isError) {
		navigate("/login");
	}

	return useMemo(() => data ?? ({} as Partial<User>), [data]);
};

export const useUpdateUser = (onSuccess?: () => void) => {
	const queryClient = useQueryClient();
	return useMutation({
		mutationFn: async (data: User) => {
			const dataRevert = await revertData(data, "imageUrl", "avatar");
			return axiosClient.put(`/user`, dataRevert);
		},
		onSuccess: () => {
			onSuccess?.();
			queryClient.invalidateQueries({
				queryKey: [USER_KEY],
			});
		},
		onError: (error: any) => {
			message.error(error.message);
		},
	});
};

export default useGetUser;
