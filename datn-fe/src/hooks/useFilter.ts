import { useCallback, useMemo } from "react";
import { useSearchParams } from "react-router-dom";
import { DEFAULT_QUERY } from "~/constants/config";
import { QueryParams } from "~/global/types";
import {
	convertQueryWithTimeRange,
	paramsSerializer,
	parseQuery,
} from "~/utils/convert";

type FilterType = Record<string, any>;

function useFilter(defaultFilter?: FilterType) {
	const [searchParams, setSearchParams] = useSearchParams();

	const filter = useMemo(() => {
		const searchParamsObj = parseQuery(searchParams.toString());
		return { ...defaultFilter, ...searchParamsObj };
	}, [searchParams]);

	const setFilter = useCallback(
		(newFilter: FilterType, keepOldValue = true) => {
			setSearchParams(
				(prevSearchParams) => {
					if (keepOldValue) {
						const prevSearchParamsObj = parseQuery(prevSearchParams.toString());
						newFilter = { ...prevSearchParamsObj, ...newFilter };
					}

					return paramsSerializer(newFilter);
				},
				{
					replace: true,
				}
			);
		},
		[setSearchParams]
	);

	return [filter, setFilter] as const;
}

export const useQueryParams = (
	defaultFilter: FilterType = DEFAULT_QUERY,
	convertQuery: (query: FilterType) => FilterType = convertQueryWithTimeRange
) => {
	const [query, setQuery] = useFilter(defaultFilter);
	const convertedQuery = useMemo(() => convertQuery(query), [query]);
	return [convertedQuery as QueryParams, setQuery] as const;
};

export default useFilter;
