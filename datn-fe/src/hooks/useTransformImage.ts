import { useQueryClient } from "@tanstack/react-query";
import { useCallback } from "react";
import { getImage } from "~/apis/upload";

type UseTransformImageParams = {
	imageUrlKey: string;
	imageFieldKey?: string;
	convert?: (data: any) => any;
	key?: string;
};

function useTransformImage({
	imageUrlKey,
	imageFieldKey = imageUrlKey,
	convert = (data) => data,
	key,
}: UseTransformImageParams) {
	const queryClient = useQueryClient();
	return useCallback(
		async (data: any) => {
			let image = data[imageUrlKey];
			if (image) {
				image = await queryClient.fetchQuery({
					queryKey: ["getImage", image],
					queryFn: () => getImage(image, key),
				});
			}
			return convert({
				...data,
				[imageFieldKey]: image,
			});
		},
		[queryClient]
	);
}

export const useTransformImageDefault = ({
	imageUrlKey = "imageUrl",
	imageFieldKey = "avatar",
	convert = (data) => ({
		...data,
		[imageFieldKey]: { url: data[imageFieldKey] },
	}),
	key,
}: Partial<UseTransformImageParams> = {}) =>
	useTransformImage({ imageUrlKey, imageFieldKey, convert, key });

export default useTransformImage;
