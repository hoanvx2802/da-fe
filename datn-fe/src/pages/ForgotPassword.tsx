import { Button, Checkbox, Form, type FormProps, Input, message } from "antd";
import { Link, useNavigate } from "react-router-dom";
import { loginApis } from "../apis/login";
import { authApis } from "~/apis/authApis";
import { useMutation } from "@tanstack/react-query";
import { axiosClient } from "~/apis/axiosConfig/axiosClient";
import { apiUrls } from "~/constants/apiUrl.constants";
import { RULES } from "~/constants/config";

type FieldType = {
	username?: string;
	password?: string;
	remember?: string;
};

function ForgotPassword() {
	const navigate = useNavigate();

	const mutation = useMutation({
		mutationFn: (values: any) => {
			const params = {
				email: values.email,
			};
			return axiosClient.post(apiUrls.forgotPassword, null, {
				params,
			});
		},
		onSuccess: (data) => {
			message.success("Gửi email thành công. Vui lòng kiểm tra email của bạn.");
			navigate("/login");
		},
		onError: (error: any) => {
			message.error(error.response.data.exception);
		},
	});

	const onFinish: FormProps<FieldType>["onFinish"] = async (values) => {
		mutation.mutate(values);
	};

	return (
		<div className="min-h-screen flex justify-center items-center bg-[url('assets/anh_nen.jpg')]">
			<Form
				className="bg-white shadow-md rounded px-10 py-8 w-full max-w-lg"
				layout="vertical"
				style={{ maxWidth: 600 }}
				initialValues={{ remember: true }}
				onFinish={onFinish}
				autoComplete="off">
				<Form.Item name="email" label="E-mail" rules={[RULES.EMAIL]}>
					<Input />
				</Form.Item>

				<Form.Item>
					<Button
						type="primary"
						htmlType="submit"
						className="login-form-button w-full h-10 p">
						Gửi mail
					</Button>
				</Form.Item>

				<Link className="underline float-right" to={"/login"}>
					Login
				</Link>
			</Form>
		</div>
	);
}

export default ForgotPassword;
