import { Button } from "antd";
import { useState } from "react";
import FilterRoom from "~/components/filter/FilterRoom";
import { useQueryParams } from "~/hooks/useFilter";
import { useOpenModal } from "~/stores/modalStore";
import ModalRoom from "../../components/ModalRoom";
import TableContent from "../../components/table/TableContent";
import useManagerRoomColumns from "./ManagerRoomColumns";
import useRooms from "./hook/useRoom";

const ManageRooms = () => {
	const showModal = useOpenModal();

	const columns = useManagerRoomColumns();
	const [totalCol, setTotalCol] = useState(columns);

	const [queryParams] = useQueryParams();
	const { data: RequestData, error } = useRooms(queryParams);
	const totalE = RequestData?.totalElements;
	const dataReq = RequestData?.data;

	return (
		<div className="mb-3 mr-3">
			<div className="flex-bw items-center pb-3">
				<p className="font-medium text-xl pr-3">Quản lý phòng</p>
				<Button
					onClick={() => showModal()}
					className="btn-primary"
					type="primary">
					Thêm phòng
				</Button>
			</div>
			<div className="flex items-center pb-3">
				<p className="text-sm pr-2">
					Tổng số phòng: <span className="font-medium">{totalE}</span>
				</p>
			</div>
			<ModalRoom />
			<FilterRoom data={dataReq} /> {/* Pass handleSearch function */}
			<TableContent
				columns={columns}
				dataSource={dataReq}
				totalCol={totalCol}
				setTotalCol={setTotalCol}
				title={"Danh sách phòng"}
			/>
		</div>
	);
};

export default ManageRooms;
