import { useQuery } from "@tanstack/react-query";
import { axiosClient } from "~/apis/axiosConfig/axiosClient";
import { apiUrls } from "~/constants/apiUrl.constants";
import { QueryParams } from "~/global/types";
interface UseRoomsParams extends Partial<QueryParams> {
	building?: string;
	roomNumber?: string;
	status?: string[] | string;
}
const useRooms = (params: UseRoomsParams) => {
	return useQuery({
		queryKey: ["UseRoom", params],
		queryFn: async () => {
			const response = await axiosClient.get(apiUrls.room, {
				params,
			});
			return response.data;
		},
	});
};

export default useRooms;
