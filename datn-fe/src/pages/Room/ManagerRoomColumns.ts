import { ColumnsType } from "antd/es/table";
import { useMemo } from "react";
import { ROOM_STATUS_OPTIONS } from "~/constants/config";
import useActionCol from "~/hooks/table/useActionCol";

const useManagerRoomColumns = () => {
	const actionCol = useActionCol({
		api: "room",
		queryKey: "UseRoom",
	});
	return useMemo(
		() =>
			[
				{
					title: "STT",
					dataIndex: "index",
					key: "index",
					render: (_: any, __: any, index: number) => index + 1,
					width: 60,
				},
				{
					title: "Số phòng",
					dataIndex: "number",
					key: "roomNumber",
					width: 150,
				},
				{
					title: "Tòa nhà",
					dataIndex: "building",
					key: "building",
					width: 150,
				},
				{
					title: "Số lượng tối đa",
					dataIndex: "capacity",
					key: "capacity",
					width: 150,
				},
				{
					title: "Số lượng hiện có",
					dataIndex: "currentQuantity",
					key: "currentQuantity",
					width: 150,
				},
				{
					title: "Giá phòng",
					dataIndex: "price",
					key: "roomPrice",
					width: 150,
				},
				{
					title: "Trạng thái",
					dataIndex: "status",
					key: "status",
					width: 150,
					render: (status: string) =>
						ROOM_STATUS_OPTIONS.find((item) => item.value === status)?.label,
				},
				{
					title: "Mô tả",
					dataIndex: "description",
					key: "description",
					width: 150,
				},
				actionCol,
			] as ColumnsType<any>,
		[actionCol]
	);
};

export default useManagerRoomColumns;
