import { ColumnsType } from "antd/es/table";
import { useMemo } from "react";
import { apiUrls } from "~/constants/apiUrl.constants";
import { STATUS_OPTIONS } from "~/constants/config";
import useActionCol from "~/hooks/table/useActionCol";
import useHandleSelectApproveActions from "~/hooks/table/useHandleSelectApproveActions";
import { getApproveActions } from "~/utils/approveActions";

const useManagerRequestColumns = () => {
	
	const onSelect = useHandleSelectApproveActions(apiUrls.requestAction, [
		"UseRequest",
	]);
	const actionCol = useActionCol({
		api: apiUrls.request,
		queryKey: "UseRequest",
		items: getApproveActions,
		onSelect,
	});
	return useMemo(
		() =>
			[
				{
					title: "STT",
					dataIndex: "index",
					key: "index",
					render: (_: any, __: any, index: number) => index + 1,
					width: 60,
				},
				{
					title: "Nội dung yêu cầu",
					dataIndex: "description",
					key: "description",
					width: 150,
				},
				{
					title: "Ngày tạo",
					dataIndex: "createAt",
					key: "createAt",
					width: 120,
				},
				{
					title: "Người tạo",
					dataIndex: "createFullName",
					width: 100,
				},
				{
					title: "Trạng thái",
					dataIndex: "status",
					key: "status",
					width: 100,
					render: (status: string) =>
						STATUS_OPTIONS.find((item) => item.value === status)
							?.label,
				},
				{
					title: "Người phê duyệt",
					dataIndex: "acceptByFull",
					key: "acceptByFull",
					width: 150,
				},
				{
					title: "Nhận xét",
					dataIndex: "comment",
					key: "comment",
					width: 150,
				},
				actionCol,
			] as ColumnsType<any>,
		[actionCol]
	);
};

export default useManagerRequestColumns;
