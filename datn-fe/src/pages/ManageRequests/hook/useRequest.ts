import { useQuery } from "@tanstack/react-query";
import { axiosClient } from "~/apis/axiosConfig/axiosClient";
import { apiUrls } from "~/constants/apiUrl.constants";
import { QueryParams } from "~/global/types";

const useRequest = (params: QueryParams, isRequest:boolean) => {
	const url = isRequest ? apiUrls.request : apiUrls.requestYc;
	const fetchRequest = () =>
		axiosClient
			.get(url, {
				params,
			})
			.then((res) => res.data);

	return useQuery({
		queryKey: ["UseRequest", params],
		queryFn: () => fetchRequest(),
	});
};

export default useRequest;
