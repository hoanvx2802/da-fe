import { ColumnsType } from "antd/es/table";
import { useMemo } from "react";
import { EMPLOYEE_POSITION_OPTIONS } from "~/constants/config";
import useActionCol from "~/hooks/table/useActionCol";
import { formatDisplayDate } from "~/utils/convert";

const useManagerEmployeeColumns = () => {
	const actionCol = useActionCol({
		api: "employee",
		queryKey: "UseEmployee",
	});
	return useMemo(
		() =>
			[
				{
					title: "STT",
					dataIndex: "index",
					key: "index",
					render: (_: any, __: any, index: number) => index + 1,
					width: 60,
				},
				{
					title: "Mã nhân viên",
					dataIndex: "employeeCode",
					key: "employeeCode",
					fixed: "left",
					width: 120,
				},
				{
					title: "Họ và tên",
					dataIndex: "fullName",
					key: "fullName",
				},
				{
					title: "Số điện thoại",
					dataIndex: "phoneNumber",
					key: "phoneNumber",
					width: 150,
				},
				{
					title: "Giới tính",
					dataIndex: "sex",
					key: "sex",
					width: 90,
					render: (_: any, record: any) => {
						return record.sex ? "Nam" : "Nữ";
					},
				},
				{
					title: "Vị trí chức vụ",
					dataIndex: "role",
					key: "role",
					width: 150,
					render: (role: string) =>
						EMPLOYEE_POSITION_OPTIONS.find((item) => item.value === role)
							?.label,
				},
				{
					title: "Ngày bắt đầu làm",
					dataIndex: "onBoard",
					key: "onBoard",
					width: 100,
					render: formatDisplayDate,
				},
				actionCol,
			] as ColumnsType<any>,
		[actionCol]
	);
};

export default useManagerEmployeeColumns;
