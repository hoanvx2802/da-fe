import { useQuery } from "@tanstack/react-query";
import { apiUrls } from "~/constants/apiUrl.constants";
import { axiosClient } from "~/apis/axiosConfig/axiosClient";
import { QueryParams } from "~/global/types";

const useEmployees = (params: QueryParams) => {
	return useQuery({
		queryKey: ["UseEmployee", params],
		queryFn: async () => {
			const response = await axiosClient.get(apiUrls.employee, {
				params,
			});
			return response.data;
		},
	});
};

export default useEmployees;
