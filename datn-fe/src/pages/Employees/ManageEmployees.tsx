import { Button } from "antd";
import { useState } from "react";
import FilterWithSearch from "~/components/filter/FilterWithSearch";
import ModalEmployee from "~/components/ModalEmployee";
import { useQueryParams } from "~/hooks/useFilter";
import { useOpenModal } from "~/stores/modalStore";
import TableContent from "../../components/table/TableContent";
import useManagerEmployeeColumns from "./ManagerEmployeeColumns";
import useEmployees from "./hook/useEmployees";

const ManageEmployees = () => {
	const showModal = useOpenModal();

	const columns = useManagerEmployeeColumns();
	const [totalCol, setTotalCol] = useState(columns);

	const [queryParams] = useQueryParams();
	const { data: RequestData, error } = useEmployees(queryParams);
	const totalE = RequestData?.totalElements;
	const dataReq = RequestData?.data;

	return (
		<div className="mb-3 mr-3">
			<div className="flex-bw items-center pb-3">
				<p className="font-medium text-xl pr-3">Quản lý nhân viên</p>
				<Button
					onClick={() => showModal()}
					className="btn-primary"
					type="primary">
					Thêm nhân viên
				</Button>
			</div>
			<div className="flex items-center pb-3">
				<p className="text-sm pr-2">
					Tổng số nhân viên: <span className="font-medium">{totalE}</span>
				</p>
			</div>
			<ModalEmployee />
			<FilterWithSearch />
			<TableContent
				columns={columns}
				dataSource={dataReq}
				totalCol={totalCol}
				setTotalCol={setTotalCol}
				title={"Danh sách nhân viên"}
			/>
		</div>
	);
};

export default ManageEmployees;
