import { useQuery } from "@tanstack/react-query";
import { axiosClient } from "../../../apis/axiosConfig/axiosClient";
import { apiUrls } from "~/constants/apiUrl.constants";
import { QueryParams } from "~/global/types";

interface Props extends QueryParams {
	month: any;
	year: any;
}

const useWaterAndElectric = (params: Props) => {
	return useQuery({
		queryKey: ["UseWaterAndElectric", params],
		queryFn: async () => {
			const response = await axiosClient.get(apiUrls.waterAndElectric, {
				params,
			});
			return response.data;
		},
	});
};

export default useWaterAndElectric;
