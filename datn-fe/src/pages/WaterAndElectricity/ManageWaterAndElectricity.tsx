import { useMutation } from "@tanstack/react-query";
import { Button, DatePicker, message } from "antd";
import dayjs from "dayjs";
import { useState } from "react";
import { axiosClient } from "~/apis/axiosConfig/axiosClient";
import ModalWaterAndElectric from "~/components/ModalWaterAndElectric";
import FilterWithSearch from "~/components/filter/FilterWithSearch";
import TableContent from "~/components/table/TableContent";
import { apiUrls } from "~/constants/apiUrl.constants";
import { DEFAULT_QUERY } from "~/constants/config";
import { authKeys } from "~/global/keys/authKeys";
import { useQueryParams } from "~/hooks/useFilter";
import useWaterAndElectricColumn from "./WaterAndElectricColumn";
import useWaterAndElectric from "./hook/useWaterAndElectric";

const ManageWaterAndElectric = () => {
	const columns = useWaterAndElectricColumn();
	const [totalCol, setTotalCol] = useState(columns);
	const [queryParams, setQueryParams] = useQueryParams({
		...DEFAULT_QUERY,
		month: dayjs().month() + 1,
		year: dayjs().year(),
	});
	const { month, year } = queryParams;
	const {
		data: RequestData,
		error,
		refetch,
	} = useWaterAndElectric(queryParams as any);
	const totalE = RequestData?.totalElectric || 0;
	const totalW = RequestData?.totalWater || 0;
	const dataReq = RequestData?.data || [];

	const handleDateChange = (date: dayjs.Dayjs | null) => {
		if (date) {
			setQueryParams({
				month: date.month() + 1, // dayjs months are 0-indexed
				year: date.year(),
			});
		} else {
			setQueryParams({
				month: undefined,
				year: undefined,
			});
		}
	};
	const mutation = useMutation({
		mutationFn: () => {
			console.log(localStorage.getItem(authKeys.username));
			const params = {
				month,
				year,
				username: localStorage.getItem(authKeys.username),
			};
			return axiosClient.post(apiUrls.fetchWAE, null, {
				params,
			});
		},
		onSuccess: () => {
			message.success("Fetch thành công");
			refetch();
		},
		onError: (error) => {
			message.error(`Fetch thất bại: ${error.message}`);
		},
	});

	return (
		<div className="mb-3 mr-3">
			<div className="flex-bw">
				<div className="flex items-center pb-3">
					<p className="font-medium text-xl pr-3">Quản lý điện nước</p>
					<DatePicker
						picker="month"
						value={dayjs()
							.set("month", month - 1)
							.set("year", year)}
						onChange={handleDateChange}
						allowClear={false}
					/>
				</div>
				<div className="flex items-center pb-3">
					<Button
						onClick={() => mutation.mutate()}
						className="btn-primary"
						type="primary">
						fetch
					</Button>
				</div>
			</div>
			<div className="flex items-center pb-3">
				<p className="text-sm pr-2">
					Tổng điện: <span className="font-medium">{totalE.toFixed(0)}</span>
				</p>
				<p className="text-sm pr-2">
					Tổng nước: <span className="font-medium">{totalW.toFixed(0)}</span>
				</p>
			</div>
			<ModalWaterAndElectric />
			<FilterWithSearch />
			<TableContent
				columns={columns}
				dataSource={dataReq}
				totalCol={totalCol}
				setTotalCol={setTotalCol}
				title={"Danh sách điện nước"}
			/>
		</div>
	);
};

export default ManageWaterAndElectric;
