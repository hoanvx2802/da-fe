import { ColumnsType } from "antd/es/table";
import { useMemo } from "react";
import useActionCol from "~/hooks/table/useActionCol";

const useWaterAndElectricColumn = () => {
  const actionCol = useActionCol({
    api: "utilities",
    queryKey: "UseWaterAndElectric",
  });
  return useMemo(
    () =>
      [
        {
          title: "STT",
          dataIndex: "index",
          key: "index",
          render: (_: any, __: any, index: number) => index + 1,
          width: 60,
        },
        {
          title: "Phòng",
          dataIndex: "number",
          key: "number",
          width: 150,
        },
        {
          title: "Tòa",
          dataIndex: "building",
          key: "building",
          width: 150,
        },
        {
          title: "Số người",
          dataIndex: "numberOfRoom",
          key: "numberOfRoom",
          width: 150,
        },
        {
          title: "Số điện đã dùng",
          dataIndex: "useNumberE",
          key: "useNumberE",
          width: 150,
          render: (_: any, record: any) => {
            const { startElectricNumber, endElectricNumber } = record;
            let p = 0;

            if (startElectricNumber === null && endElectricNumber === null) {
				p = 0;
			} else if (startElectricNumber === null) {
				// If startElectricNumber is null and endElectricNumber is not null, p should be NaN
				p = NaN;
			} else if (endElectricNumber === null) {
				// If endElectricNumber is null and startElectricNumber is not null, p should be 0
				p = 0;
			} else {
				// If both values are not null, calculate p as endElectricNumber - startElectricNumber
				p = parseFloat(endElectricNumber) - parseFloat(startElectricNumber);
			}
            return p;
          },
        },
        {
          title: "Số nước đã dùng",
          dataIndex: "useNumberW",
          key: "useNumberW",
          width: 150,
          render: (_: any, record: any) => {
            const { startWaterNumber, endWaterNumber } = record;
            let p = 0;
            if (startWaterNumber === null && endWaterNumber === null) {
				p = 0;
			} else if (startWaterNumber === null) {
				// If startElectricNumber is null and endElectricNumber is not null, p should be NaN
				p = NaN;
			} else if (endWaterNumber === null) {
				// If endElectricNumber is null and startElectricNumber is not null, p should be 0
				p = 0;
			} else {
				// If both values are not null, calculate p as endElectricNumber - startElectricNumber
				p = parseFloat(endWaterNumber) - parseFloat(startWaterNumber);
			}
            return p;
          },
        },
        {
          title: "Tiền điện",
          dataIndex: "electricPrice",
          key: "electricPrice",
          width: 150,
        },
        {
          title: "Tiền nước",
          dataIndex: "waterPrice",
          key: "waterPrice",
          width: 150,
        },
        {
          title: "Tiền phòng",
          dataIndex: "roomPrice",
          key: "roomPrice",
          width: 150,
        },
        {
          title: "Tổng tiền",
          dataIndex: "totalPrice",
          key: "totalPrice",
          width: 150,
          render: (_: any, record: any) => {
            const { electricPrice, waterPrice, roomPrice } = record;
            const total =
              (electricPrice ? parseFloat(electricPrice) : 0) +
              (waterPrice ? parseFloat(waterPrice) : 0) +
              (roomPrice ? parseFloat(roomPrice) : 0);
            return total;
          },
        },
        actionCol,
      ] as ColumnsType<any>,
    [actionCol]
  );
};

export default useWaterAndElectricColumn;
