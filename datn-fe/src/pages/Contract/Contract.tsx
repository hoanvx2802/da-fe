import { Button, Select } from "antd";
import { useState } from "react";
import FilterWithTimeRange from "~/components/filter/FilterWithTimeRange";
import ModalContract from "~/components/ModelContract";
import { useQueryParams } from "~/hooks/useFilter";
import { useOpenModal } from "~/stores/modalStore";
import TableContent from "../../components/table/TableContent";
import useContractColumns from "./ContractColumns";
import useContract from "./hook/useContract";
import FilterField from "~/components/form/FilterField";
import { CONTRACT_STATUS_OPTIONS, STATUS_OPTIONS } from "~/constants/config";

const Contract = () => {
  const showModal = useOpenModal();
  const columns = useContractColumns();
  const [totalCol, setTotalCol] = useState(columns);

  const [queryParams] = useQueryParams();
  const { data: RequestData, error } = useContract(queryParams, true);
  const dataReq = RequestData?.data;

  return (
    <div className="mb-3 mr-3">
      <div className="flex-bw items-center pb-3">
        <p className="font-medium text-xl pr-3">Quản lý hợp đồng</p>
        <Button
          onClick={() => showModal()}
          className="btn-primary"
          type="primary"
        >
          Thêm hợp đồng
        </Button>
      </div>
      <ModalContract />
      <FilterWithTimeRange
        children={
          <>
            <FilterField name="status">
				<Select
					className="w-full"
					mode="multiple"
					placeholder="Chọn trạng thái"
					allowClear
					options={CONTRACT_STATUS_OPTIONS as any}
				/>
			</FilterField>
          </>
        }
      />
      <TableContent
        columns={columns}
        dataSource={dataReq}
        totalCol={totalCol}
        setTotalCol={setTotalCol}
        title={"Danh sách hợp đồng"}
      />
    </div>
  );
};

export default Contract;
