import { ColumnsType } from "antd/es/table";
import { useMemo } from "react";
import { apiUrls } from "~/constants/apiUrl.constants";
import useActionCol from "~/hooks/table/useActionCol";
import { getActionsDependStatus } from "~/utils/approveActions";
import { managerColumns } from "../ManagerContract/constants";

function useContractColumns() {
	const actionCol = useActionCol({
		api: apiUrls.contract,
		queryKey: "UseContract",
		items: getActionsDependStatus,
	});
	return useMemo(
		() => [...managerColumns, actionCol] as ColumnsType<any>,
		[actionCol]
	);
}

export default useContractColumns;
