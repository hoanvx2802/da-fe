import { useQuery } from "@tanstack/react-query";
import { axiosClient } from "~/apis/axiosConfig/axiosClient";
import { apiUrls } from "~/constants/apiUrl.constants";
import { QueryParams } from "~/global/types";
interface UseRoomsParams extends QueryParams {
	building?: string;
	roomNumber?: string;
	status?: Set<string>;
}
const useContract = (params: UseRoomsParams, isContract: boolean) => {
	const url = isContract ? apiUrls.contract : apiUrls.contractYc;
	return useQuery({
		queryKey: ["UseContract", params, isContract],
		queryFn: async () => {
			const response = await axiosClient.get(url, {
				params,
			});
			return response.data;
		},
	});
};

export default useContract;
