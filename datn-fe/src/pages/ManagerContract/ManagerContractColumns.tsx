import { ColumnsType } from "antd/es/table";
import { useMemo } from "react";
import { apiUrls } from "~/constants/apiUrl.constants";
import useActionCol from "~/hooks/table/useActionCol";
import useHandleSelectApproveActions from "~/hooks/table/useHandleSelectApproveActions";
import { getApproveActions } from "~/utils/approveActions";
import { managerColumns } from "./constants";

const useManagerContractColumns = () => {
	const onSelect = useHandleSelectApproveActions(apiUrls.acceptContract, [
		"UseContract",
	]);
	const actionCol = useActionCol({
		api: apiUrls.contract,
		queryKey: "UseContract",
		items: getApproveActions,
		onSelect,
	});
	return useMemo(
		() => [...managerColumns, actionCol] as ColumnsType<any>,
		[actionCol]
	);
};

export default useManagerContractColumns;
