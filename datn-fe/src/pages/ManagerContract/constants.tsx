import { STATUS_OPTIONS } from "~/constants/config";

export const managerColumns = [
	{
		title: "STT",
		dataIndex: "index",
		key: "index",
		render: (_: any, __: any, index: number) => index + 1,
		width: 60,
	},
	{
		title: "Số phòng",
		dataIndex: "roomNumber",
		key: "roomNumber",
		width: 150,
	},
	{
		title: "Tòa nhà",
		dataIndex: "building",
		key: "building",
		width: 150,
	},
	{
		title: "Sinh viên",
		dataIndex: "studentFullName",
		key: "studentFullName",
		width: 150,
	},
	{
		title: "Người tạo",
		dataIndex: "employeeFullName",
		key: "employeeFullName",
		width: 150,
	},
	{
		title: "Trạng thái",
		dataIndex: "status",
		key: "status",
		width: 150,
		render: (status: string) =>
			STATUS_OPTIONS.find((item) => item.value === status)
				?.label,
	},
	{
		title: "Ngày bắt đầu",
		dataIndex: "startAt",
		key: "startAt",
		width: 150,
	},
	{
		title: "Ngày kết thúc",
		dataIndex: "endAt",
		key: "endAt",
		width: 150,
	},
	{
		title: "Người phê duyệt",
		dataIndex: "acceptByFull",
		key: "acceptByFull",
		width: 150,
	},
	
];
