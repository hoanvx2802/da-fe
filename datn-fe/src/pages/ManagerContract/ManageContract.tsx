import { Select, Tabs } from "antd";
import { useState } from "react";
import AppRangePicker from "~/components/fields/AppRangePicker";
import { CONTRACT_STATUS_OPTIONS, DEFAULT_QUERY } from "~/constants/config";
import { useQueryParams } from "~/hooks/useFilter";
import TableContent from "../../components/table/TableContent";
import useManagerContractColumns from "./ManagerContractColumns";
import useContract from "../Contract/hook/useContract";
import FilterWithTimeRange from "~/components/filter/FilterWithTimeRange";
import ModalManagerContract from "~/components/ModalManagerContract";
import FilterManagerWithTimeRange from "~/components/filter/FilterManagerWithTimeRange";
import FilterField from "~/components/form/FilterField";

const ManageContract = () => {
  const columns = useManagerContractColumns();
  const [totalCol, setTotalCol] = useState(columns);

  const [queryParams, setQueryParams] = useQueryParams({
    ...DEFAULT_QUERY,
    isAccept: false,
  });
  const { data: RequestData, error } = useContract(queryParams, false);
  const dataReq = RequestData?.data;
  const totalE = RequestData?.totalElements;
  const [activeTab, setActiveTab] = useState(Number(queryParams.isAccept));

  return (
    <div className="mb-3 mr-3">
      <div className="flex-bw items-center pb-3">
        <p className="font-medium text-xl pr-3">Quản lý hợp đồng</p>
      </div>
      <div className="flex items-center pb-3">
        <p className="text-sm pr-2">
          Tổng số hợp đồng: <span className="font-medium">{totalE}</span>
        </p>
      </div>

      <Tabs
        activeKey={Number(queryParams.isAccept) + ""}
        onChange={(key) => {
          setQueryParams({ isAccept: !!Number(key) });
          setActiveTab(Number(key));
        }}
        items={[
          {
            key: "0",
            label: "Chờ xử lý",
          },
          {
            key: "1",
            label: "Đã duyệt",
          },
        ]}
      />
      {activeTab === 1 ? (
        <FilterWithTimeRange
          children={
            <>
              <FilterField name="status">
                <Select
                  className="w-full"
                  mode="multiple"
                  placeholder="Chọn trạng thái"
                  allowClear
                  options={CONTRACT_STATUS_OPTIONS as any}
                />
              </FilterField>
            </>
          }
        />
      ) : (
        <FilterWithTimeRange />
      )}

      <ModalManagerContract />

      <TableContent
        columns={columns}
        dataSource={dataReq}
        totalCol={totalCol}
        setTotalCol={setTotalCol}
        title={"Danh sách hợp đồng"}
      />
    </div>
  );
};

export default ManageContract;
