import { Button } from "antd";
import { useState } from "react";
import FilterWithSearch from "~/components/filter/FilterWithSearch";
import { useQueryParams } from "~/hooks/useFilter";
import { useOpenModal } from "~/stores/modalStore";
import TableContent from "../../components/table/TableContent";
import useManagerSystemColumn from "./ManagerSystemColumn";
import useSystem from "./hook/useSystem";
import ModalSystem from "~/components/ModalSystem";

const ManageSystem = () => {
	const showModal = useOpenModal();

	const columns = useManagerSystemColumn();
	const [totalCol, setTotalCol] = useState(columns);

	const [queryParams] = useQueryParams(); // Assuming queryParams would be relevant for students as well
	const { data: RequestData, error } = useSystem(queryParams);
	const dataReq = RequestData?.data;

	return (
		<div className="mb-3 mr-3">
			<div className="flex-bw items-center pb-3">
				<p className="font-medium text-xl pr-3">Quản lý cấu hình</p>
				<Button
					onClick={() => showModal()}
					className="btn-primary"
					type="primary">
					Thêm cấu hình
				</Button>
			</div>
			<ModalSystem />
			<FilterWithSearch />

			<TableContent
				columns={columns}
				dataSource={dataReq}
				totalCol={totalCol}
				setTotalCol={setTotalCol}
				title={"Danh sách cấu hình"}
			/>
		</div>
	);
};

export default ManageSystem;
