import { useQuery } from "@tanstack/react-query";
import { axiosClient } from "~/apis/axiosConfig/axiosClient";
import { apiUrls } from "~/constants/apiUrl.constants";
import { QueryParams } from "~/global/types";

const useSystem = (params: QueryParams) => {
	return useQuery({
		queryKey: ["UseSystem", params],
		queryFn: async () => {
			const response = await axiosClient.get(apiUrls.system, {
				params,
			});
			return response.data;
		},
	});
};

export default useSystem;
