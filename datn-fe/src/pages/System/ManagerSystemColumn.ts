import { ColumnsType } from "antd/es/table";
import { useMemo } from "react";
import useActionCol from "~/hooks/table/useActionCol";

const useManagerSystemColumn = () => {
	const actionCol = useActionCol({
		api: "system",
		queryKey: "UseSystem",
	});
	return useMemo(
		() =>
			[
				{
					title: "STT",
					dataIndex: "index",
					key: "index",
					render: (_: any, __: any, index: number) => index + 1,
					width: 60,
				},
				{
					title: "Khóa",
					dataIndex: "keyword",
					key: "keyword",
					fixed: "left",
					width: 150,
				},
				{
					title: "Giá trị",
					dataIndex: "value",
					key: "value",
					width: 250,
				},
				actionCol,
			] as ColumnsType<any>,
		[actionCol]
	);
};

export default useManagerSystemColumn;
