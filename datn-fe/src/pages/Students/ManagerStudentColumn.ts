import { ColumnsType } from "antd/es/table";
import { useMemo } from "react";
import useActionCol from "~/hooks/table/useActionCol";

const useManagerStudentColumn = () => {
	const actionCol = useActionCol({
		api: "student",
		queryKey: "UseStudent",
	});
	return useMemo(
		() =>
			[
				{
					title: "STT",
					dataIndex: "index",
					key: "index",
					render: (_: any, __: any, index: number) => index + 1,
					width: 60,
				},
				{
					title: "Mã sinh viên",
					dataIndex: "studentCode",
					key: "studentCode",
					fixed: "left",
					width: 120,
				},
				{
					title: "Họ và tên",
					dataIndex: "fullName",
					key: "fullName",
				},
				{
					title: "Số điện thoại",
					dataIndex: "phoneNumber",
					key: "phoneNumber",
					width: 150,
				},
				{
					title: "Giới tính",
					dataIndex: "sex",
					key: "sex",
					width: 90,
					render: (_: any, record: any) => {
						return record.sex ? "Nam" : "Nữ";
					},
				},
				{
					title: "Trạng thái xếp phòng",
					dataIndex: "status",
					key: "status",
					width: 150,
				},
				{
					title: "Lớp",
					dataIndex: "className",
					key: "className",
					width: 150,
				},
				{
					title: "Khoa",
					dataIndex: "department",
					key: "department",
					width: 150,
				},
				actionCol,
			] as ColumnsType<any>,
		[actionCol]
	);
};

export default useManagerStudentColumn;
