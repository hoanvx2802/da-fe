import { useQuery } from "@tanstack/react-query";
import { apiUrls } from "~/constants/apiUrl.constants";
import { QueryParams } from "~/global/types";
import { axiosClient } from "../../../apis/axiosConfig/axiosClient";
interface UseStudentParams extends Partial<QueryParams> {
	
}

const useStudent = (params?: UseStudentParams) => {
	return useQuery({
		queryKey: ["UseStudent", params],
		queryFn: async () => {
			const response = await axiosClient.get(apiUrls.student, {
				params,
			});
			return response.data;
		},
	});
};

export default useStudent;
