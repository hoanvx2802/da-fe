import { Button } from "antd";
import { useState } from "react";
import { useQueryParams } from "~/hooks/useFilter";
import { useOpenModal } from "~/stores/modalStore";
import ModalStudent from "../../components/ModalStudent";
import TableContent from "../../components/table/TableContent";
import useManagerStudentColumn from "./ManagerStudentColumn";
import useStudent from "./hook/useStudents";
import FilterWithSearch from "~/components/filter/FilterWithSearch";

const ManageStudents = () => {
	const showModal = useOpenModal();

	const columns = useManagerStudentColumn();
	const [totalCol, setTotalCol] = useState(columns);

	const [queryParams] = useQueryParams();
	const { data: RequestData, error } = useStudent(queryParams);
	const totalE = RequestData?.totalElements;
	const dataReq = RequestData?.data;

	return (
		<div className="mb-3 mr-3">
			<div className="flex-bw items-center pb-3">
				<p className="font-medium text-xl pr-3">Quản lý sinh viên</p>
				<Button
					onClick={() => showModal()}
					className="btn-primary"
					type="primary">
					Thêm sinh viên
				</Button>
			</div>
			<div className="flex items-center pb-3">
				<p className="text-sm pr-2">
					Tổng sinh viên: <span className="font-medium">{totalE}</span>
				</p>
			</div>
			<ModalStudent />
			<FilterWithSearch />

			<TableContent
				columns={columns}
				dataSource={dataReq}
				totalCol={totalCol}
				setTotalCol={setTotalCol}
				title={"Danh sách sinh viên"}
			/>
		</div>
	);
};

export default ManageStudents;
