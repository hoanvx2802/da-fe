import { Button, Select } from "antd";
import { useState } from "react";
import ModalRequest from "~/components/ModalRequest";
import { useQueryParams } from "~/hooks/useFilter";
import { useOpenModal } from "~/stores/modalStore";
import TableContent from "../../components/table/TableContent";
import useRequest from "../ManageRequests/hook/useRequest";
import useRequestColumns from "./RequestColumns";
import FilterWithTimeRange from "~/components/filter/FilterWithTimeRange";
import FilterField from "~/components/form/FilterField";
import { CONTRACT_STATUS_OPTIONS } from "~/constants/config";

const Request = () => {
  const showModal = useOpenModal();

  const columns = useRequestColumns();
  const [totalCol, setTotalCol] = useState(columns);

  const [queryParams] = useQueryParams();
  const { data: RequestData, error } = useRequest(queryParams, true);
  const dataReq = RequestData?.data;

  return (
    <div className="mb-3 mr-3">
      <div className="flex-bw items-center pb-3">
        <p className="font-medium text-xl pr-3">Yêu cầu</p>
        <Button
          onClick={() => showModal()}
          className="btn-primary"
          type="primary"
        >
          Thêm yêu cầu
        </Button>
      </div>
      <ModalRequest />
      <FilterWithTimeRange
        children={
          <>
            <FilterField name="status">
              <Select
                className="w-full"
                mode="multiple"
                placeholder="Chọn trạng thái"
                allowClear
                options={CONTRACT_STATUS_OPTIONS as any}
              />
            </FilterField>
          </>
        }
		placeholder="Nhập tên người duyệt"
      />
      <TableContent
        columns={columns}
        dataSource={dataReq}
        totalCol={totalCol}
        setTotalCol={setTotalCol}
        title={"Danh sách yêu cầu"}
      />
    </div>
  );
};

export default Request;
