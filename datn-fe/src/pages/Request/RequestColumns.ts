import { ColumnsType } from "antd/es/table";
import { useMemo } from "react";
import { apiUrls } from "~/constants/apiUrl.constants";
import { CONTRACT_STATUS_OPTIONS, STATUS_OPTIONS } from "~/constants/config";
import useActionCol from "~/hooks/table/useActionCol";
import { getActionsDependStatus } from "~/utils/approveActions";

const useRequestColumns = () => {
	const actionCol = useActionCol({
		api: apiUrls.request,
		queryKey: "UseRequest",
		items: getActionsDependStatus,
	});
	return useMemo(
		() =>
			[
				{
					title: "STT",
					dataIndex: "index",
					key: "index",
					render: (_: any, __: any, index: number) => index + 1,
					width: 60,
				},
				{
					title: "Nội dung yêu cầu",
					dataIndex: "description",
					key: "description",
				},
				{
					title: "Ngày tạo",
					dataIndex: "createAt",
					key: "createAt",
					width: 100,
				},
				{
					title: "Người tạo",
					dataIndex: "createFullName",
					width: 100,
				},
				{
					title: "Trạng thái",
					dataIndex: "status",
					key: "status",
					width: 100,
					render: (status: string) =>
						STATUS_OPTIONS.find((item) => item.value === status)
							?.label,
				},
				{
					title: "Người phê duyệt",
					dataIndex: "acceptByFull",
					key: "acceptBy",
					width: 150,
				},
				{
					title: "Nhận xét",
					dataIndex: "comment",
					key: "comment",
					width: 150,
				},
				actionCol,
			] as ColumnsType<any>,
		[actionCol]
	);
};

export default useRequestColumns;
