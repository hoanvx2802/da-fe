import { useQuery } from "@tanstack/react-query";
import { axiosClient } from "~/apis/axiosConfig/axiosClient";
import { apiUrls } from "~/constants/apiUrl.constants";
import { QueryParams } from "~/global/types";

const useCiCout = (params: QueryParams) => {
	const fetchRequest = () =>
		axiosClient
			.get(apiUrls.cicout, {
				params,
			})
			.then((res) => res.data);

	return useQuery({
		queryKey: ["UseCiCout", params],
		queryFn: () => fetchRequest(),
	});
};

export default useCiCout;
