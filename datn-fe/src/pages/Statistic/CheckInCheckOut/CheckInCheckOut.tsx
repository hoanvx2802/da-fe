import { useState } from "react";
import FilterWithTimeRange from "~/components/filter/FilterWithTimeRange";
import ModalCiCout from "~/components/ModalCiCout";
import TableContent from "~/components/table/TableContent";
import { useQueryParams } from "~/hooks/useFilter";
import useCheckInCheckOutColumns from "./CheckInCheckOutColumns";
import useCiCout from "./hook/useCiCout";

const CheckInCheckOut = () => {
	const columns = useCheckInCheckOutColumns();
	const [totalCol, setTotalCol] = useState(columns);
	const [queryParams] = useQueryParams();

	const { data: RequestData, error, refetch } = useCiCout(queryParams as any);
	const dataReq = RequestData?.data || [];

	return (
		<div className="mb-3 mr-3">
			<div className="flex-bw">
				<div className="flex items-center pb-3">
					<p className="font-medium text-xl pr-3">Quản lý vào ra</p>
				</div>
			</div>
			<ModalCiCout />
			<FilterWithTimeRange />
			<TableContent
				columns={columns}
				dataSource={dataReq}
				totalCol={totalCol}
				setTotalCol={setTotalCol}
				title={"Danh sách vào ra"}
			/>
		</div>
	);
};

export default CheckInCheckOut;
