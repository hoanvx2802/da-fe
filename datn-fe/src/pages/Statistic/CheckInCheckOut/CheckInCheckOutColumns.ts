import { ColumnsType } from "antd/es/table";
import { useMemo } from "react";
import { TABLE_ACTION_ITEMS } from "~/constants/config";
import useActionCol from "~/hooks/table/useActionCol";
import { formatDateTime } from "~/utils/convert";

const items = [TABLE_ACTION_ITEMS.DETAIL];

const useCheckInCheckOutColumns = () => {
	const actionCol = useActionCol({
		api: "checkInCheckOut",
		queryKey: "useCiCout",
		items,
	});
	return useMemo(
		() =>
			[
				{
					title: "STT",
					dataIndex: "index",
					key: "index",
					render: (_: any, __: any, index: number) => index + 1,
					width: 60,
				},
				{
					title: "Tên sinh viên",
					dataIndex: "studentFullName",
					key: "studentFullName",
					fixed: "left",
				},
				{
					title: "Thời gian",
					dataIndex: "timeCheck",
					width: 150,
					render: formatDateTime,
				},
				{
					title: "Trạng thái",
					dataIndex: "status",
					width: 150,
				},
				actionCol,
			] as ColumnsType<any>,
		[actionCol]
	);
};

export default useCheckInCheckOutColumns;
