import { Button, Checkbox, Form, type FormProps, Input, message } from "antd";
import { Link, useNavigate } from "react-router-dom";
import { loginApis } from "../apis/login";
import { authKeys } from "../global/keys/authKeys";
import { LockOutlined, UserOutlined } from "@ant-design/icons";
import { RULES } from "~/constants/config";
import { useMutation } from "@tanstack/react-query";

interface info {
	username: string;
	displayName: string;
	token: string;
	refreshToken: string;
	role: any;
}

export const gotoLogin = () => {
	localStorage.clear();
	window.location.href = `${import.meta.env.VITE_FE_BASE_URL}`;
};

export const getRole = () => {
	const storedData = localStorage.getItem(authKeys.user);
	if (!storedData) {
		return null;
	}
	const user = JSON.parse(storedData);
	return user.role[0].authority;
};

export const getDisplayName = async () => {
	const storedData = localStorage.getItem(authKeys.user);
	if (!storedData) {
		return null;
	}
	const user = storedData ? JSON.parse(storedData) : null;
	return user.displayName;
};

function Login() {
	const navigate = useNavigate();
	const mutation = useMutation({
		mutationFn: loginApis.login,
		onSuccess: (data: info) => {
			localStorage.setItem(authKeys.user, JSON.stringify(data));
			localStorage.setItem(authKeys.accessToken, data.token);
			localStorage.setItem(authKeys.username, data.username);
			navigate("/");
		},
		onError: (error: any) => {
			localStorage.clear();
			message.error(error.exception);
		},
	});

	return (
		<div className="min-h-screen flex justify-center items-center bg-[url('assets/anh_nen.jpg')]">
			<Form
				className="login-form bg-white shadow-md rounded px-10 pt-8 w-full max-w-lg"
				name="normal_login"
				initialValues={{ remember: true }}
				onFinish={mutation.mutate}>
				<Form.Item name="username" rules={[RULES.REQUIRED]}>
					<Input
						prefix={<UserOutlined className="site-form-item-icon h-10" />}
						placeholder="Username"
					/>
				</Form.Item>
				<Form.Item name="password" rules={[RULES.REQUIRED]}>
					<Input.Password
						prefix={<LockOutlined className="site-form-item-icon h-10" />}
						placeholder="Password"
					/>
				</Form.Item>
				<Form.Item>
					<Form.Item name="remember" valuePropName="checked" noStyle>
						<Checkbox>Ghi nhớ</Checkbox>
					</Form.Item>

					<Link className="login-form-forgot" to={"/forgot_password"}>
						Quên mật khẩu?
					</Link>
				</Form.Item>

				<Form.Item>
					<Button
						type="primary"
						htmlType="submit"
						className="login-form-button w-full h-10 p"
						loading={mutation.isPending}>
						Đăng nhập
					</Button>
				</Form.Item>
			</Form>
		</div>
	);
}

export default Login;
