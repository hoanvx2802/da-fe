export type QueryParams = {
	page: number;
	limit: number;
	keyword?: string;
	[key: string]: any;
};
