export const authKeys = {
	token: "sessionToken",
	mappingMenus: "mappingMenus",
	accessToken: "accessToken",
	user: "user",
	username: "username"
} as const;
