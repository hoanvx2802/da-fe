import { Modal } from "antd";
import { HookAPI } from "antd/es/modal/useModal";
import { createContext, ReactNode, useContext } from "react";

const ModalFormContext = createContext<HookAPI>({} as HookAPI);

function ModalFormProvider({ children }: { children: ReactNode }) {
	const [modal, contextHolder] = Modal.useModal();
	return (
		<ModalFormContext.Provider value={modal}>
			{children}
			{contextHolder}
		</ModalFormContext.Provider>
	);
}

export const useModalWithContext = () => {
	return useContext(ModalFormContext);
};

export default ModalFormProvider;
