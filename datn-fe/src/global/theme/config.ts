import { FormProps } from "antd";

export const FORM_LAYOUT: FormProps = {
	labelCol: { span: 7 },
	labelAlign: "left",
};

export const FORM_VALIDATE_MESSAGES = {
	default: "${label} không hợp lệ!",
	required: "${label} không được để trống!",
	enum: "${label} phải thuộc một trong các giá trị sau: ${enum}!",
	whitespace: "${label} không được để trống!",
	date: {
		format: "${label} không đúng định dạng ngày tháng!",
		parse: "${label} không thể chuyển đổi thành ngày tháng!",
		invalid: "${label} không phải là ngày tháng hợp lệ!",
	},
	types: {
		string: "${label} phải là chuỗi!",
		method: "${label} phải là hàm!",
		array: "${label} phải là mảng!",
		object: "${label} phải là đối tượng!",
		number: "${label} phải là số!",
		date: "${label} phải là ngày tháng!",
		boolean: "${label} phải là boolean!",
		integer: "${label} phải là số nguyên!",
		float: "${label} phải là số thực!",
		regexp: "${label} không đúng định dạng!",
		email: "${label} không đúng định dạng email!",
		url: "${label} không đúng định dạng URL!",
		hex: "${label} phải là mã màu HEX!",
	},
	string: {
		len: "${label} phải chứa đúng ${len} ký tự!",
		min: "${label} phải chứa ít nhất ${min} ký tự!",
		max: "${label} phải chứa nhiều nhất ${max} ký tự!",
		range: "${label} phải chứa từ ${min} đến ${max} ký tự!",
	},
	number: {
		len: "${label} phải bằng ${len}!",
		min: "${label} phải lớn hơn hoặc bằng ${min}!",
		max: "${label} phải nhỏ hơn hoặc bằng ${max}!",
		range: "${label} phải nằm trong khoảng từ ${min} đến ${max}!",
	},
	array: {
		len: "Phải chứa đúng ${len} ${label}!",
		min: "Phải chứa ít nhất ${min} ${label}!",
		max: "Phải chứa nhiều nhất ${max} ${label}!",
		range: "Phải chứa từ ${min} đến ${max} ${label}!",
	},
	pattern: {
		mismatch: "${label} không khớp với định dạng ${pattern}!",
	},
};
