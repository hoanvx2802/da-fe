import { ConfigProvider } from "antd";
import React from "react";
import { FORM_VALIDATE_MESSAGES } from "./config";
import ModalFormProvider from "./ModalFormProvider";

type Props = {
	children: React.ReactNode;
};

function ThemeProvider({ children }: Props) {
	return (
		<ConfigProvider
			form={{
				validateMessages: FORM_VALIDATE_MESSAGES,
			}}>
			<ModalFormProvider>{children}</ModalFormProvider>
		</ConfigProvider>
	);
}

export default ThemeProvider;
