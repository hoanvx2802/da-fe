import {
	AreaChartOutlined,
	AuditOutlined,
	BarsOutlined,
	FileProtectOutlined,
	FileTextOutlined,
	PlusCircleOutlined,
	TeamOutlined,
	UserOutlined,
} from "@ant-design/icons";
import { ROLE } from "~/constants/config";
import Contract from "~/pages/Contract/Contract";
import ManageEmployees from "~/pages/Employees/ManageEmployees.tsx";
import ManageContract from "~/pages/ManagerContract/ManageContract";
import ManageRequests from "~/pages/ManageRequests/ManageRequests.tsx";
import Request from "~/pages/Request/Request";
import ManageRooms from "~/pages/Room/ManageRooms.tsx";
import CheckInCheckOut from "~/pages/Statistic/CheckInCheckOut/CheckInCheckOut.tsx";
import ManageStudents from "~/pages/Students/ManageStudent.tsx";
import ManageSystem from "~/pages/System/ManagerSystem";
import ManageWaterAndElectricity from "~/pages/WaterAndElectricity/ManageWaterAndElectricity.tsx";

export type AppRoute = {
	label: string;
	path: string;
	element: JSX.Element;
	icon: JSX.Element;
	roles?: string[];
	children?: AppRoute[];
};

const appRoutes: AppRoute[] = [
	{
		label: "Quản lý phòng",
		path: "/manager_rooms",
		element: <ManageRooms />,
		icon: <BarsOutlined />,
		roles: [ROLE.ADMINISTRATIVE, ROLE.MANAGEMENT_FINANCIAL],
	},
	{
		label: "Quản lý nhân viên",
		path: "/manage_employees",
		element: <ManageEmployees />,
		icon: <UserOutlined />,
	},
	{
		label: "Quản lý sinh viên",
		path: "/manage_students",
		element: <ManageStudents />,
		icon: <TeamOutlined />,
		roles: [ROLE.ADMINISTRATIVE, ROLE.MANAGEMENT_FINANCIAL],
	},
	{
		label: "Thống kê hóa đơn",
		path: "/invoice",
		element: <ManageWaterAndElectricity />,
		icon: <FileProtectOutlined />,
		roles: [ROLE.MANAGEMENT_FINANCIAL],
	},
	{
		label: "Thống kê ra vào",
		path: "/statistic/check_in_check_out",
		element: <CheckInCheckOut />,
		icon: <AreaChartOutlined />,
		roles: [ROLE.ADMINISTRATIVE, ROLE.STUDENT],
	},
	{
		label: "Yêu cầu",
		path: "/request",
		element: <Request />,
		icon: <PlusCircleOutlined />,
		roles: [ROLE.STUDENT, ROLE.MANAGEMENT_FINANCIAL, ROLE.ADMINISTRATIVE],
	},
	{
		label: "Quản lý yêu cầu",
		path: "/manage_requests",
		element: <ManageRequests />,
		icon: <AuditOutlined />,
		roles: [ROLE.MANAGEMENT_FINANCIAL, ROLE.ADMINISTRATIVE],
	},
	{
		label: "Hợp đồng",
		path: "/contract",
		element: <Contract />,
		icon: <FileTextOutlined />,
		roles: [ROLE.ADMINISTRATIVE],
	},
	{
		label: "Quản lý hợp đồng",
		path: "/manager_contracts",
		element: <ManageContract />,
		icon: <FileTextOutlined />,
	},
	{
		label: "Cấu hình hệ thống",
		path: "/system",
		element: <ManageSystem />,
		icon: <FileTextOutlined />,
		roles: [ROLE.MANAGEMENT_FINANCIAL, ROLE.ADMINISTRATIVE],
	},
];

export default appRoutes;
