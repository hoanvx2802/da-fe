import { Menu } from "antd";
import { useEffect, useState } from "react";
import { Link, useLocation } from "react-router-dom";
import { AppRoute } from "../router";

export type MainSideBarProps = {
	items: AppRoute[];
};

const MainSideBar = ({ items }: MainSideBarProps) => {
	const location = useLocation();
	const defaultKey =
		location.pathname === "/" ? "/manager_rooms" : location.pathname;
	const [selectedKeys, setSelectedKeys] = useState<string[]>([defaultKey]);

	useEffect(() => {
		const path =
			location.pathname === "/" ? "/manager_rooms" : location.pathname;
		setSelectedKeys([path]);
	}, [location.pathname]);

	return (
		<Menu
			mode="inline"
			selectedKeys={selectedKeys}
			defaultSelectedKeys={selectedKeys}
			style={{ height: "100%", borderRight: 0 }}>
			{items.map((item) =>
				item.children ? (
					<Menu.SubMenu key={item.path} icon={item.icon} title={item.label}>
						{item.children.map((child) => (
							<Menu.Item key={child.path} icon={child.icon}>
								<Link to={child.path || "#"}>{child.label}</Link>
							</Menu.Item>
						))}
					</Menu.SubMenu>
				) : (
					<Menu.Item key={item.path} icon={item.icon}>
						<Link to={item.path || "#"}>{item.label}</Link>
					</Menu.Item>
				)
			)}
		</Menu>
	);
};

export default MainSideBar;
