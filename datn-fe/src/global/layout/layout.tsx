import { Layout } from "antd";
import { Outlet, useLocation } from "react-router-dom";
import DashboardHeader from "~/components/DashboardHeader";
import MainSideBar, { MainSideBarProps } from "./MainSidebar";

const { Header, Content, Sider } = Layout;

function MainLayout({ items }: MainSideBarProps) {
  const { pathname } = useLocation();
  const currentPageLabel =
    items.find((item) => item.path === pathname)?.label || "";

  return (
    <Layout className="h-screen w-screen">
      <Header className="px-5 bg-white">
        <DashboardHeader keyValue={currentPageLabel} />
      </Header>
      <Layout>
        <Sider width={220} theme="light">
          <MainSideBar items={items} />
        </Sider>
        <Content className="px-6 py-9">
          <Outlet />
        </Content>
      </Layout>
    </Layout>
  );
}

export default MainLayout;
