import React, { useMemo } from "react";
import { Navigate, RouteObject, useRoutes } from "react-router-dom";
import { ROLE } from "~/constants/config";
import MainLayout from "~/global/layout/layout";
import appRoutes, { AppRoute } from "~/global/router";
import useGetUser from "~/hooks/useGetUser";
import ForgotPassword from "~/pages/ForgotPassword";
import Login from "~/pages/Login";

const App: React.FC = () => {
	const user = useGetUser();
	const role = user.role;

	const routes = useMemo(() => {
		const mainRoutes: RouteObject[] = [];
		appRoutes.forEach(({ roles, ...other }) => {
			if (role === ROLE.ADMIN || roles?.includes(role)) {
				mainRoutes.push({ ...other });
			}
		});
		if (mainRoutes[0]) {
			mainRoutes.unshift({
				index: true,
				element: <Navigate to={mainRoutes[0].path!} replace />,
			});
		}
		return [
			{
				path: "/",
				element: <MainLayout items={mainRoutes as AppRoute[]} />,
				children: mainRoutes,
			},
			{ path: "/login", element: <Login /> },
			{ path: "/forgot_password", element: <ForgotPassword /> },
		];
	}, [role]);

	return useRoutes(routes);
};

export default App;
